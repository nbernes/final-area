const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    Area = mongoose.model('Area');


exports.handleTriggeredArea = async function (actionid, callback, actionBody) {

    await Area.findOne({actionid: actionid}).then(function (area) {
        if (area === null)
            return callback(area);
        Reaction.findOne({_id:area.reactionid}).then(function (reaction) {
            console.log(reaction);
            Service.findOne({_id:reaction.owner}).then(function (service) {
                console.log(service);
                let request = require('request');
                let options = {
                    'method': 'POST',
                    'url': process.env.SERVER_URL + 'services/' + service.name + '/reaction/' + area.reactionid,
                    'headers': {},
                };
                request(options, function (error, response) {
                    if (error) throw new Error(error);
                    console.log(response.body);
                });
            });

        });
    })
};