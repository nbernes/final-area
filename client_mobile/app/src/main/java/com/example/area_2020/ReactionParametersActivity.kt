package com.example.area_2020

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.widget.addTextChangedListener
import com.example.area_2020.communication.extern_post
import com.example.area_2020.data.areacreaData
import com.example.area_2020.data.globalApp
import okhttp3.FormBody
import org.json.JSONObject

class ReactionParametersActivity : AppCompatActivity() {

    var data: JSONObject = areacreaData.Companion.parameters_react

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_reaction_parameters)

        var listParameter = getListParameter(data)
        aff(listParameter)

        var validbtn = findViewById<CardView>(R.id.card_valid_reaction_parameters)

        validbtn.setOnClickListener {
            var valid = true

            for (i in 0..(listParameter.size - 1)) {
                if (listParameter[i].value == "") {
                    valid = false
                }
            }
            if (valid == false) {
                Toast.makeText(this, "Arguments are missing", Toast.LENGTH_SHORT).show()
            } else {
                var parameters = JSONObject()
                for (i in 0..(listParameter.size -1)) {
                    parameters.put(listParameter[i].key, listParameter[i].value)
                }
                val requestBody = FormBody.Builder().add("userToken", globalApp.Companion.userToken).add("serviceName", areacreaData.Companion.service_react).add("reactionName", areacreaData.Companion.name_react).add("parameters", parameters.toString()).build()
                val response = extern_post(globalApp.subdomain + "/user/reaction/new", requestBody)
                if (response.code == 200) {
                    response.mess = response.mess.replace("\"", "")
                    areacreaData.Companion.id_react = response.mess
                    val arearequestbody = FormBody.Builder().add("userToken", globalApp.Companion.userToken).add("actionId", areacreaData.Companion.id_act).add("reactionId", areacreaData.Companion.id_react).build()
                    val arearesponse =  extern_post(globalApp.Companion.subdomain + "/user/area/new", arearequestbody)
                    println("AREARESPONSE : " +arearesponse)
                    if (arearesponse.code == 200) {
                        Toast.makeText(this, "Area Successfully Created", Toast.LENGTH_SHORT).show()
                        finish()
                    } else  {
                        Toast.makeText(this, response.mess, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, response.mess, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun aff(listParameter: ArrayList<ModelReactionParameter>)
    {
        val listview = findViewById<ListView>(R.id.listview_reaction_parameters)
        val adapter = MyReactionParameterAdapter(this, listParameter)
        listview.adapter = adapter
    }

    inner class MyReactionParameterAdapter(context : Context, listParameter: ArrayList<ModelReactionParameter>) : BaseAdapter() {
        private val data = listParameter
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val parameterReactionItem = layoutInflater.inflate(R.layout.item_parameter, viewGroup, false)

            val recup = parameterReactionItem.findViewById<EditText>(R.id.txt_parametre)

            if (data[position].desc != "")
                recup.setHint(data[position].desc)
            else
                recup.setHint(data[position].key)
            recup.addTextChangedListener {
                data[position].value = recup.text.toString()
            }
            return parameterReactionItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

    }

    fun getListParameter(parameterList: JSONObject) : ArrayList<ModelReactionParameter> {
        var listParameter = ArrayList<ModelReactionParameter>()

        val keys = parameterList.keys()
        val listkey = keys.asSequence().toList()

        for (i in 0..(listkey.size -1)) {
            var param = ModelReactionParameter("", "", "")
            param.key = listkey[i]
            param.desc = parameterList.getString(listkey[i])
            listParameter.add(param)
        }
        return listParameter
    }
}

data class ModelReactionParameter(
    var key : String,
    var desc : String,
    var value : String
)