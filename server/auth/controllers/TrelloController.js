const OAuth = require('oauth').OAuth;
const {newService} = require("../../User/controllers/UserController");

const requestURL = "https://trello.com/1/OAuthGetRequestToken";
const accessURL = "https://trello.com/1/OAuthGetAccessToken";
const authorizeURL = "https://trello.com/1/OAuthAuthorizeToken";
const appName = "Area";
const scope = 'read,write';

var oauth_secrets = {};
var  oauth;
var userToken;
var toRedirect;
exports.callOAuth = async function (req, res) {
    const key = process.env.TRELLO_CONSUMER_KEY;
    const secret = process.env.TRELLO_CONSUMER_SECRET;
    const loginCallback =  process.env.SERVER_URL+'auth/trello/callback';

    oauth = new OAuth(requestURL, accessURL, key, secret, "1.0A", loginCallback, "HMAC-SHA1");
    userToken = req.query.userToken;
    toRedirect = req.query.toRedirect
    oauth.getOAuthRequestToken(function (error, token, tokenSecret, results) {
        oauth_secrets[token] = tokenSecret;
        res.redirect(`${authorizeURL}?oauth_token=${token}&name=${appName}&scope=${scope}`);
    });
};

exports.checkAuthentication = async function (req, res) {
    const key = process.env.TRELLO_CONSUMER_KEY;
    const secret = process.env.TRELLO_CONSUMER_SECRET;
    const loginCallback =  process.env.SERVER_URL+'auth/trello/callback';
 oauth = new OAuth(requestURL, accessURL, key, secret, "1.0A", loginCallback, "HMAC-SHA1");

    const {oauth_token, oauth_verifier} = req.query;
    oauth.getOAuthAccessToken(oauth_token, oauth_secrets[oauth_token], oauth_verifier, function (error, accessToken, accessTokenSecret, result) {
        newService(userToken, "trello", accessToken, accessTokenSecret).then(res => console.log(res)).catch();

    });
    res.redirect("http://localhost:8081/" + toRedirect);
};