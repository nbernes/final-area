const {handleTriggeredArea} = require("../controllers/AreaController");

const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');
const crypto = require('crypto');
const config = require('./config/webhook.config');

exports.createWebHook = async function (req, res) {
    const {userToken, actionId} = req.body;
    let currentUser;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send('Invalid user');
        currentUser = user;
    }).catch();
    let serviceData;
    await Service.findOne({name: "github", owner: currentUser._id}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(401).send('No service github for user');
        serviceData = service;
    });
    let actionToCreate;
    await Action.findOne({_id: actionId}).then(function (action) {
        if (action === undefined || action === null)
            return res.status(400).send("Error creating action");
        actionToCreate = action;
    });

    // console.log(actionToCreate);
    let webhookConfig = config;

    webhookConfig.events.push(actionToCreate.name);
    webhookConfig.config.secret = actionToCreate._id;
    webhookConfig.config.url = process.env.SERVER_URL + "services/github/action";
    let parameters = JSON.parse(actionToCreate.parameters);
    let URLD = 'https://api.github.com/repos/' + parameters.owner + '/' + parameters.repo + '/hooks';
    let request = require('request');
    let options = {
        'method': 'POST',
        'url': URLD,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': 'token ' + serviceData.accessToken,
            'User-Agent': 'AreaGithub'
        },
        body: JSON.stringify(webhookConfig)

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });

};

exports.handleWebHook = async function (req, res) {
    const blob = JSON.stringify(req.body);
    const theirSignature = req.get('X-Hub-Signature');

    let services;
    await Service.find({name: "github"}).then(function (servicesFound) {
        if (servicesFound.length === 0)
            return res.status(400).send('No github services');
        services = servicesFound;
    });

    for (let i = 0; i < services.length; i++) {

        let actions = services[i].actionsid;
        for (let j = 0; j < actions.length; j++) {

            const hmac = crypto.createHmac('sha1', actions[j]._id.toString());
            const ourSignature = `sha1=${hmac.update(blob).digest('hex')}`;
            const bufferA = Buffer.from(ourSignature, 'utf8');
            const bufferB = Buffer.from(theirSignature, 'utf8');
            const safe = crypto.timingSafeEqual(bufferA, bufferB);

            if (safe) {
                await handleTriggeredArea(actions[j]._id, function (err) {
                    console.log(err);
                });
            } else {

            }
        }
    }
};

exports.handleReaction = async function (req, res) {
    const reactionId = req.params.reactionid;

    let reaction;
    await Reaction.findOne({_id: reactionId}).then(function (react) {

        if (react === undefined || react === null)
            return res.status(400).send("Reaction not found");
        reaction = react;
    }).catch();
    let service;
    await Service.findOne({_id: reaction.owner}).then(function (serv) {
        if (serv === undefined || serv === null)
            return res.status(400).send("Error service not found");
        service = serv;
    });
    let reg = new RegExp("/:owner\/:repo");
    let match = reg.exec(reaction.route);

    let parameters = JSON.parse(reaction.parameters);

    console.log("PARAMS GITHUB HAHA: ", parameters);
    if (match !== null) {
        reaction.route = reaction.route.replace(":owner", parameters.owner);
        reaction.route = reaction.route.replace(":repo", parameters.repo);
        delete parameters["owner"];
        delete parameters["repo"];
    }

    let request = require('request');
    let options = {
        'method': 'POST',
        'url': 'https://api.github.com' + reaction.route,
        'headers': {
            'Authorization': 'token ' + service.accessToken,
            'Content-Type': 'application/json',
            'User-Agent': 'AreaGithub'
        },
        body: JSON.stringify(parameters)
    };

    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log("Hook create");
        console.log(response.body);
    });


    return res.status(200).send();
};