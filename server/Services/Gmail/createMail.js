const {google} = require('googleapis');
const mailComposer = require('nodemailer/lib/mail-composer');
	
class CreateMail {

	constructor(auth, me, to, task, subject, corpus) {
		this.me = me;
		this.task = task;
		this.auth = auth;
		this.to = to;
		this.sub = subject;
		this.body = corpus;
		this.gmail = google.gmail({version: 'v1', auth});
	}

	makeBody() {
		let mail = new mailComposer({
			to: this.to,
			text: this.body,
			subject: this.sub,
			textEncoding: "base64"
		});

		mail.compile().build((err, msg) => {
			if (err) {
				return console.log('Error compiling email ' + error);
			}
			const encodedMessage = Buffer.from(msg)
				.toString('base64')
				.replace(/\+/g, '-')
				.replace(/\//g, '_')
				.replace(/=+$/, '');
			if (this.task === 'mail') {
				this.sendMail(encodedMessage);
			}
		});
	}

	sendMail(encodedMessage) {
		this.gmail.users.messages.send({
			userId: this.me,
			resource: {
				raw: encodedMessage,
			}
		}, (err, result) => {
			if (err) {
				return console.log('NODEMAILER - The API returned an error: ' + err);
			}

			console.log("NODEMAILER - Sending email reply from server:", result.data);
		});
	}
}
module.exports = CreateMail;
