# Client #
* host: 10.101.53.35
# Server #
* ## Services ##
* ### Reactions ###
  * name: post_message
* #### Parameters ####
  * description: The user post a message
  * name: twitter
* ### Actions ###
  * name: new_message_from_user
  * description: A new message is posted from a specific user
  * name: new_message_inbox
  * description: A new private message is received by the user
  * name: new_hashword_notification
  * description: A new message with a specific #word was posted
  * description: 
* ### Reactions ###
  * name: post_message_in_server_channel
  * description: The user post a message in a specified server channel
  * name: discord
* ### Actions ###
  * name: new_message_from_user
  * description: A new private message is received by the user
  * name: new_notification_from_server_channel
  * description: A message is posted in a channel server
  * description: 
* ### Reactions ###
  * name: send_mail
  * description: The user send a new mail
  * name: google_mail
* ### Actions ###
  * name: new_mail_received
  * description: A new mail is received by the user
  * description: 
* ### Reactions ###
  * name: create_new_file
  * description: The user create a new file
  * name: upload_file
  * description: The user upload a  file
  * name: create_new_directory
  * description: The user create a new directory
  * name: dropbox
* ### Actions ###
  * name: new_file_is_added
  * description: A new file is added to a directory
  * name: file_is_modified
  * description: A file is modified
  * name: directory_is_modified
  * description: A directory is modified
  * name: new_directory_is_added
  * description: A new directory is added
  * description: 
* ### Reactions ###
  * name: create_new_event
  * description: The user create a new event
  * name: google_calendar
* ### Actions ###
  * name: new_event_created
  * description: A new event is created by the user
  * name: event_modified
  * description: An event is modified by the user
  * name: event_date_soon
  * description: An event is happening soon
  * description: 
* ### Reactions ###
  * name: create_new_card_in_list
  * description: Create a new card in list
  * name: create_new_list
  * description: Create a new list
  * name: trello
* ### Actions ###
  * name: new_card_added_to_list
  * description: A new card is added to a list
  * name: new_list_created
  * description: A new list is created
  * description: 
* ### Reactions ###
  * name: ebay
* ### Actions ###
  * name: followed_offer_sold
  * description: Offer followed by user has been sold
  * name: new_offer_for_followed_item
  * description: An offer for a wanted item appear
  * description: 
* ### Reactions ###
  * route: /repos/:owner/:repo/issues
  * name: issues
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * body: content of the issue
  * title: title of the issue
  * description: create an issue
  * route: /user/repos
  * name: repos
* #### Parameters ####
  * name: name of the repository
  * private: boolean
  * description: project description
  * description: create a repository
  * route: /repos/:owner/:repo/pulls
  * name: pulls
* #### Parameters ####
  * body: Description of the pull request
  * head: username:branch
  * title: The title of the new pull request
  * repo: name of the repository
  * base: existing branch to request the change to be pulled into
  * owner: username of the repository
  * description: create a pull request on repository
  * api_url: https://api.github.com/
  * name: github
* ### Actions ###
  * name: create
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: A new branch or tag is created
  * name: issue_comment
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: An issue is commented or a comment is edited
  * name: issue
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: An issue has been created or updated  in any way
  * name: milestone
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: A milestone is created or edited in any way
  * name: pull_request
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: A pull request is created or updated in any way
  * name: push
* #### Parameters ####
  * owner: user name of the repository
  * repo: name of the repository
  * description: A push on the repository 
  * description: 
* current_time: 1531680780
