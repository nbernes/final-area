package com.example.area_2020

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.areacreaData
import com.example.area_2020.data.globalApp
import com.example.area_2020.ui.new_area.ModelService
import com.squareup.picasso.Picasso
import okhttp3.HttpUrl
import org.json.JSONArray

class ReactionServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_reaction_service)

        val serviceTab = getServiceTab()
        aff(serviceTab)
    }


    fun aff(serviceTab : ArrayList<ModelService>) {
        val txt = findViewById<TextView>(R.id.no_service_text_react)
        val listview = findViewById<ListView>(R.id.listview_service_react)
        val adapter = MyServiceAdapter(this, serviceTab)
        val clicklistener = MyOwnClickListener(this, serviceTab)

        if (serviceTab.size == 0) {
            txt.visibility = View.VISIBLE
            listview.visibility = View.INVISIBLE
        } else {
            txt.visibility = View.INVISIBLE
            listview.visibility = View.VISIBLE
            listview.adapter = adapter
            listview.onItemClickListener = clicklistener
        }
    }

    inner class MyOwnClickListener(context: Context, serviceTab: ArrayList<ModelService>) : AdapterView.OnItemClickListener {
        private val data = serviceTab

        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (data[position].isConnected == false) {
                areacreaData.Companion.title_connect = data[position].name
                areacreaData.Companion.creafrom = 2
                val intent = Intent(this@ReactionServiceActivity, LoginActivity::class.java)
                startActivity(intent)
            } else {
                areacreaData.Companion.service_react = data[position].name
                val intent = Intent(this@ReactionServiceActivity, ReactionActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    inner class MyServiceAdapter(context : Context, serviceTab: ArrayList<ModelService>) : BaseAdapter() {
        private val data = serviceTab
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val serviceItem = layoutInflater.inflate(R.layout.item_service, viewGroup, false)

            val txt_name = serviceItem.findViewById<TextView>(R.id.servicename)
            val txt_status = serviceItem.findViewById<TextView>(R.id.connectstatus)
            val img = serviceItem.findViewById<ImageView>(R.id.servicelogo)

            Picasso.get().load(data[position].image).into(img)
            txt_name.setText(data[position].name)


            if (data[position].isConnected == false) {
                txt_status.setText("Not Connected")
                txt_status.setTextColor(Color.RED)
            } else {
                txt_status.setText("Connected")
                txt_status.setTextColor(Color.GREEN)
            }

            return serviceItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

    }

    fun getServiceTab() : ArrayList<ModelService> {
        val arrayService : ArrayList<ModelService> = ArrayList()
        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("services").addPathSegment("list").build()
        var response = extern_get(url)


        if (response.code == 200) {
            println("LIST SERVICE : " + response.mess)
            val jsonservice = JSONArray(response.mess)
            for (i in 0..(jsonservice.length() -1)) {
                var service : ModelService = ModelService("", "", "", false)

                service.name = jsonservice.getJSONObject(i).getString("name")
                service.image = jsonservice.getJSONObject(i).getString("logo")
                service.connectionRequired = jsonservice.getJSONObject(i).getString("connectionRequired")
                service.isConnected = false
                arrayService.add(service)
            }
            url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("service").addQueryParameter("userToken", globalApp.Companion.userToken).build()
            response = extern_get(url)
            if (response.code == 200) {
                val jsonuserservice = JSONArray(response.mess)
                for (i in 0..(jsonuserservice.length() -1)) {
                    for (j in 0..(arrayService.size - 1)) {
                        if (jsonuserservice.getJSONObject(i).getString("name") == arrayService[j].name) {
                            arrayService[j].isConnected = true
                        }
                    }
                }
            } else {
                Toast.makeText(this, "Can't access to the server", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Can't access to the server", Toast.LENGTH_SHORT).show()
        }
        return arrayService
    }
}