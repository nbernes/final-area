package com.example.area_2020

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.widget.addTextChangedListener
import com.example.area_2020.communication.extern_post
import com.example.area_2020.data.areacreaData
import com.example.area_2020.data.globalApp
import okhttp3.FormBody
import org.json.JSONObject
import java.lang.reflect.Array

class ActionParametersActivity : AppCompatActivity() {

    var data: JSONObject = areacreaData.Companion.parameters_act

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_action_parameter)

        var listParameter = getListParameter(data)
        aff(listParameter)
        var validbtn = findViewById<CardView>(R.id.card_valid_action_parameters)

        validbtn.setOnClickListener {
            var valid = true

            for (i in 0..(listParameter.size -1))    {
                if (listParameter[i].value == "") {
                    valid = false
                }
            }
            if (valid == false) {
                Toast.makeText(this, "Arguments are missing", Toast.LENGTH_SHORT).show()
            } else {
                var parameters = JSONObject()
                for (i in 0..(listParameter.size -1)) {
                    parameters.put(listParameter[i].key, listParameter[i].value)
                }
                val requestBody = FormBody.Builder().add("userToken", globalApp.Companion.userToken).add("serviceName", areacreaData.Companion.service_act).add("actionName", areacreaData.Companion.name_act).add("parameters", parameters.toString()).build()
                println("REQUETE : "+ requestBody.toString())
                val response = extern_post(globalApp.Companion.subdomain + "/user/action/new", requestBody)
                if (response.code == 200) {
                    response.mess = response.mess.replace("\"", "")
                    areacreaData.Companion.id_act = response.mess
                    val intent = Intent(this@ActionParametersActivity, ReactionServiceActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(this, response.mess, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun aff(listParameter: ArrayList<ModelActionParameter>)
    {
        val listview = findViewById<ListView>(R.id.listview_action_parameters)
        val adapter = MyActionParameterAdapter(this, listParameter)
        listview.adapter = adapter
    }

    inner class MyActionParameterAdapter(context : Context, listParameter: ArrayList<ModelActionParameter>) : BaseAdapter() {
        private val data = listParameter
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val parameterActionItem = layoutInflater.inflate(R.layout.item_parameter, viewGroup, false)

            val recup = parameterActionItem.findViewById<EditText>(R.id.txt_parametre)

            if (data[position].desc != "")
                recup.setHint(data[position].desc)
            else
                recup.setHint(data[position].key)
            recup.addTextChangedListener {
                data[position].value = recup.text.toString()
            }
            return parameterActionItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

    }

    fun getListParameter(parameterList: JSONObject) : ArrayList<ModelActionParameter> {
        var listParameter = ArrayList<ModelActionParameter>()

        val keys = parameterList.keys()
        val listkey = keys.asSequence().toList()

        for (i in 0..(listkey.size -1)) {
            var param = ModelActionParameter("", "", "")
            param.key = listkey[i]
            param.desc = parameterList.getString(listkey[i])
            listParameter.add(param)
        }
        return listParameter
    }
}



data class ModelActionParameter(
    var key : String,
    var desc : String,
    var value : String
    )