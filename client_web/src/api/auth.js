import axios from 'axios'

const reqHeader = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Access-Control-Allow-Origin": process.env.VUE_APP_PUBLIC_SERVER_URL,
    // "Access-Control-Request-Method": "POST"
};

export default axios.create({
    baseURL: process.env.VUE_APP_LOCAL_SERVER_URL + 'auth/',
    headers: reqHeader,
    redirect: 'follow',
    crossDomain: true,
    method: "POST"
});