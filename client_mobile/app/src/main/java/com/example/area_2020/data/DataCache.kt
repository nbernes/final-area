package com.example.area_2020.data

import android.content.Context
import android.content.SharedPreferences

class DataCache(private val context: Context) {

    private val USER_TOKEN = ""
    private val IS_LOGIN = "false"
    private val app_prefs: SharedPreferences

    init {
        app_prefs = context.getSharedPreferences("shared", Context.MODE_PRIVATE)
    }

    fun putUserToken(data:String) {
        val edit = app_prefs.edit()
        edit.putString(USER_TOKEN, data)
        edit.commit()
    }

    fun getUserToken() : String? {
        return app_prefs.getString(USER_TOKEN, "")
    }

    fun putIsLogin(data:String) {
        val edit = app_prefs.edit()
        edit.putString(IS_LOGIN, data)
        edit.commit()
    }

    fun getIsLogin() : String? {
        return app_prefs.getString(IS_LOGIN, "false")
    }
}