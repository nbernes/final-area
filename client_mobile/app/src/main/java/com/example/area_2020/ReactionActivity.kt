package com.example.area_2020

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.areacreaData
import com.example.area_2020.data.globalApp
import okhttp3.HttpUrl
import org.json.JSONArray
import org.json.JSONObject

class ReactionActivity : AppCompatActivity() {

    var data: String = areacreaData.service_react

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_reaction)

        val txt_title = findViewById<TextView>(R.id.reaction_title)


        txt_title.setText("Reaction for " + data)
        var listReaction = getListReaction(data)

        aff(listReaction)
    }

    fun aff(listReaction : ArrayList<ModelReaction>) {
        val listview = findViewById<ListView>(R.id.listview_reaction)
        val txt_no_service = findViewById<TextView>(R.id.no_service_set_text_react)
        val adapter = MyServiceAdapter(this, listReaction)
        val onclick = MyServiceClickListener(this, listReaction)

        if (listReaction.size == 0) {
            txt_no_service.visibility = View.VISIBLE
            listview.visibility = View.INVISIBLE
        } else {
            listview.visibility = View.VISIBLE
            txt_no_service.visibility = View.INVISIBLE
            listview.adapter = adapter
            listview.onItemClickListener = onclick
        }
    }

    inner class MyServiceClickListener(context : Context, listReaction : ArrayList<ModelReaction>) : AdapterView.OnItemClickListener {
        private val data = listReaction

        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            areacreaData.Companion.parameters_react = data[position].parameters
            areacreaData.Companion.name_react = data[position].name
            val intent = Intent(this@ReactionActivity,ReactionParametersActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    inner class MyServiceAdapter(context : Context, listReaction: ArrayList<ModelReaction>) : BaseAdapter() {
        private val data = listReaction
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val actionItem = layoutInflater.inflate(R.layout.item_actionreaction, viewGroup, false)

            val txt_desc = actionItem.findViewById<TextView>(R.id.action_desc)

            txt_desc.setText(data[position].desc)

            return actionItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }
    }

    fun getListReaction(serviceName : String) : ArrayList<ModelReaction> {
        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("services").addPathSegment(data).addPathSegment("reaction").addQueryParameter("userToken", globalApp.Companion.userToken).build()
        var response = extern_get(url)
        println("REACTION RESPONSE : " + response)
        if (response.code == 200) {
            var jsonList = ArrayList<ModelReaction>()
            var jsonAct = JSONArray(response.mess)
            for (i in 0..(jsonAct.length()-1)) {
                var reaction = ModelReaction("", "", "", "", JSONObject())
                reaction.id = jsonAct.getJSONObject(i).getString("_id")
                reaction.owner = jsonAct.getJSONObject(i).getString("owner")
                reaction.name = jsonAct.getJSONObject(i).getString("name")
                reaction.desc = jsonAct.getJSONObject(i).getString("description")
                reaction.parameters = JSONObject(jsonAct.getJSONObject(i).getString("parameters"))

                jsonList.add(reaction)
            }
            return jsonList
        } else {
            Toast.makeText(this, "Can't access to the server", Toast.LENGTH_SHORT).show()
        }
        return ArrayList<ModelReaction>()
    }
}

data class ModelReaction(
    var id : String,
    var owner : String,
    var name : String,
    var desc : String,
    var parameters : JSONObject
    )