// import servicesAPI from '../../api/services'

const state = {
    action: {},
    reaction: {},
    actionSettings: {},
    reactionSettings: {}
};

// getters
const getters = {
    areActionSettingsSet: state => {
        for (let [key, value] of Object.entries(state.actionSettings)) {
            if (key !== 'req' && value === '')
                return false;
        }
        return true;
    },
    areReactionSettingsSet: state => {
        for (let [key, value] of Object.entries(state.reactionSettings))
            if (key !== 'req' && value === '')
                return false;
        return true;
    }
};

// mutations
const mutations = {
    setBaseActionSettings(state, settingsParams) {
        state.actionSettings = settingsParams;
         for (let key of Object.keys(settingsParams)) {
            state.actionSettings[key] = '';
        }
    },
    setBaseReactionSettings(state, settingsParams) {
        state.reactionSettings = settingsParams;
        for (let key of Object.keys(settingsParams)) {
            state.reactionSettings[key] = '';
        }
    },
    setActionSetting(state, setting) {
        state.actionSettings[setting.key] = setting.value;
    },
    setReactionSetting(state, setting) {
        state.reactionSettings[setting.key] = setting.value;
    },
    setAction(state, item) {
        state.action = item;
    },
    setReaction(state, item) {
        state.reaction = item;
    }
};

// actions
const actions = {
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}