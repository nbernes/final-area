import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user'
import area from './modules/area'
import services from "./modules/services";
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        count: 0
    },
    modules: {
        user : user,
        services: services,
        area: area
    },
    plugins: [
        createPersistedState({
            paths: ['user']
        })
    ]
});