module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    port: 8080,
    proxy: 'https://area2020.ngrok.io/',
  }
};
