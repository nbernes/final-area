const OAuth = require('oauth').OAuth;
const {newService} = require("../../User/controllers/UserController");
const util = require('util');
const requestURL = "https://api.twitter.com/oauth/request_token";
const accessURL = "https://api.twitter.com/oauth/access_token";

const loginCallback = 'http://127.0.0.1:8080/auth/twitter/callback';
var oauth;

var userToken;
var inspect = require('util-inspect');
var token;
var secrettoken;
var toRedirect;

exports.callOAuth = async function (req, res) {
    oauth = new OAuth(requestURL, accessURL, process.env.TWITTER_CONSUMER_KEY, process.env.TWITTER_CONSUMER_SECRET, "1.0A", loginCallback, "HMAC-SHA1");
    userToken = req.query.userToken;
    toRedirect = req.query.toRedirect;
    console.log(oauth);
    oauth.getOAuthRequestToken(function (error, oauthToken, oauthTokenSecret, results) {
        console.log(results);
        console.log(error);
        if (error) {
            res.status(500).send("Error getting OAuth request token : " + util.inspect(error));
        } else {
            req.session.oauthRequestToken = oauthToken;
            req.session.oauthRequestTokenSecret = oauthTokenSecret;

            token = oauthToken;
            secrettoken = oauthTokenSecret;
            console.log(oauthToken);
            res.redirect("https://twitter.com/oauth/authorize?oauth_token=" + req.session.oauthRequestToken);
        }
    });
};

exports.checkAuthentication = async function (req, res) {

    oauth.getOAuthAccessToken(token, secrettoken, req.query.oauth_verifier, function (error, oauthAccessToken, oauthAccessTokenSecret, results) {
        if (error) {
            res.send("Error getting OAuth access token : " + inspect(error) + "[" + oauthAccessToken + "]" + "[" + oauthAccessTokenSecret + "]" + "[" + inspect(results) + "]", 500);
        } else {
            req.session.oauthAccessToken = oauthAccessToken;
            req.session.oauthAccessTokenSecret = oauthAccessTokenSecret;

            newService(userToken, "twitter", oauthAccessToken, oauthAccessTokenSecret).then(res => console.log(res)).catch();
            res.redirect("http://localhost:8081/" + toRedirect);
        }
    })
};