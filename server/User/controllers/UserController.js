const bcrypt = require("bcryptjs");
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Area = mongoose.model('Area');
const Action = mongoose.model('Action');
const Service = mongoose.model('Service');
const Reaction = mongoose.model('Reaction');


async function getUser(userToken, callback) {
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === null || user === undefined)
            return (callback("Error invalid user"));
        return callback(user);
    })
}

async function getService (owner = "area", serviceName = "", callback) {
    if (serviceName === "")
        await Service.find({owner: owner}, function (err, service) {
            if (service === null || service === undefined)
                return callback("Error service");
            return callback(service);
        });
    else
        await Service.findOne({owner: owner, name: serviceName}, function (err, service) {
            if (service === null || service === undefined)
                return callback("Error service not found");
            return callback(service);
        });
}

async function getUserService(userToken, callback) {
    await getUser(userToken, function (ret) {
        if (ret.toString().search('Error') !== -1)
            return callback(ret);
        getService(ret._id, "", function (ret) {
            return callback(ret);
        });
    });
}

async function getUserServiceName (userToken, serviceName, callback) {
    await getUser(userToken, function (ret) {
        if (ret.toString().search('Error') !== -1)
            return callback(ret);
        getService(ret._id, serviceName, function (ret) {
            return callback(ret);
        });
    });
}

async function deleteService(owner, serviceName, callback) {
    await getService(owner, serviceName, function (ret) {
        console.log(ret.toString().search('Error'));
        if (ret.toString().search('Error') !== -1)
            return callback(ret);
        else {
            Action.find({owner: ret._id}).then(function (actions) {
                for (let i = 0; i < actions.length; i++) {
                    actions[i].delete();
                }
            });
            Reaction.find({owner: ret._id}).then(function (reactions) {
                for (let i = 0; i < reactions.length; i++) {
                    reactions[i].delete();
                }
            });
            ret.delete();
            callback("Success service deleted");
        }
    })
}

/**
 * @route GET /user
 * @group user
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @returns {object} 200 - user information
 * @returns {Error} 400 - invalid token
 */
exports.getUser = async function (req, res) {
    const {userToken} = req.query;

    await getUser(userToken, function (ret) {
        if (ret.toString().search('Error') !== -1)
            return res.status(401).send(ret);
        return res.status(200).json(ret);
    })
};

exports.updateUserInfo = async function (req, res) {
  const {userToken, email, name} = req.body;

  await getUser(userToken, function(ret) {
    if (ret.toString().search('Error') !== -1)
        return res.status(401).send(ret);
    if (email !== undefined && email != null)
      ret.email = email;
    if (name !== undefined && name != null)
      ret.name = name;
    ret.save();
    return res.status(200).send("Successfully change info");
  })
}

async function changePassword(user, password) {
  const saltRounds = 10;
  let salt = await bcrypt.genSalt(saltRounds);
  ret.password = await bcrypt.hash(password, salt);
  ret.save();
  return res.status(200).send("Successfully change password");

}

exports.changePassword = async function (req, res) {
  const {userToken, password} = req.body;

  if (password == undefined || password == null) 
    console.log("dafuq")

  await getUser(userToken, function (ret) {
    if (ret.toString().search('Error') !== -1)
        return res.status(401).send(ret);
    if (password == undefined || password == null) 
        return res.status(401).send("Can't have empty password");
    changePassword(ret, password);
  })
}

/**
 * This list existing user's services
 * @route GET /user/service
 * @group service
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @returns {object} 200 - array of service in a json format
 * @returns {Error} 401 - Error
 */
exports.service = async function (req, res) {
    const {userToken} = req.query;

    await getUserService(userToken, function (ret) {
        if (ret.toString().search('Error') !== -1)
            return res.status(401).send(ret);
        return res.status(200).json(ret);
    });
};

/**
 * This list existing user's services
 * @route GET /user/service/:serviceid
 * @group service
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @returns {object} 200 - service in a json format
 * @returns {Error} 401 - Error
 */
exports.serviceByName = async function (req, res) {
    const {userToken} = req.body;

    await getUserServiceName(userToken, req.params.serviceid, function (ret) {
        if (ret.toString().search('Error') !== -1)
            return res.status(401).send(ret);
        return res.status(200).json(ret);
    });
};

/**
 * This add a service to the user
 * @route POST /user/service/new
 * @group service
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} serviceName.x-www-form-urlencoded.required - service name to add
 * @param {string} accessToken.x-www-form-urlencoded.required - service user access token
 * @param {string} refreshToken.x-www-form-urlencoded - service user refresh token
 * @returns {object} 200 - Service added to user
 * @returns {Error} 401 - No service for serviceName
 * @returns {Error} 400 - Service already exist for user
 */
exports.newService = async function (userToken, serviceName, accessToken, refreshToken, userId = '') {
    await getUserServiceName(userToken, serviceName, function (ret) {
        if (ret.toString().search('Error') === -1)
            return (ret);
        let serviceInfo;
        getService("area", serviceName, function (ret) {
            if (ret.toString().search('Error') !== -1)
                return (ret);
            serviceInfo = ret;
            getUser(userToken, function (user) {
                if (ret.toString().search('Error') !== -1)
                    return ret;
                let service = new Service({
                        name: serviceName,
                        owner: user._id,
                        accessToken: accessToken,
                        refreshToken: refreshToken,
                        userid: userId
                    }
                );
                user.userServices.push(service._id);
                service.save();
                user.save();
                return (200);
            });
        });
    });
};

/**
 * This delete a service for user
 * @route DELETE /user/service/delete
 * @group service
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} serviceName.x-www-form-urlencoded.required - service name to delete
 * @returns {object} 200 - Service deleted
 * @returns {Error} 400 - Error
 */
exports.deleteService = async function (req, res) {
    const {serviceName, userToken} = req.query;

    await getUser(userToken, function (user) {
        deleteService(user._id, serviceName, function (ret) {
            if (ret.toString().search('Error') !== -1)
                return res.status(400).send(ret);
            return res.status(200).json(ret);
        })
    });
};

/**
 * @route POST /user/action/new
 * @group action
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} serviceName.x-www-form-urlencoded.required - service name for action to add
 * @param {string} actionName.x-www-form-urlencoded.required - action name to add
 * @param {string} parameters.x-www-form-urlencoded - parameters of action in form of json string
 * @returns {object} 200 - actionid created
 * @returns {Error} 400 - Error
 */
exports.newAction = async function (req, res) {
    const {userToken, serviceName, actionName, parameters} = req.body;
    let currentUser;
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        currentUser = user;
    }).catch();
    let currentService;
    await Service.findOne({name: serviceName, owner: currentUser._id}).then(function (service) {
        if (service === null)
            return res.status(400).send("service not found");
        currentService = service;
    }).catch();
    let newAction = new Action({
        name: actionName,
        owner: currentService._id,
        parameters: parameters // JSON.stringify(parameters)
    });
    newAction.save();
    currentService.actionsid.push(newAction._id);
    currentService.save();

    const request = require('request');
    let options = {
        'method': 'POST',
        'url': process.env.SERVER_URL + 'services/' + currentService.name + '/action/new',
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            'userToken': currentUser.authToken,
            'actionId': newAction._id.toString()
        }
    };
    try {
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
        });
    } catch (err) {
        return res.status(400).send(err);
    }
    return res.status(200).send(newAction._id);
};

/**
 * @route POST /user/reaction/new
 * @group reaction
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} serviceName.x-www-form-urlencoded.required - service name for action to add
 * @param {string} reactionName.x-www-form-urlencoded.required - reaction name to add
 * @param {string} parameters.x-www-form-urlencoded - parameters of action in form of json string
 * @returns {object} 200 - reactionid created
 * @returns {Error} 400 - Error
 */
exports.newReaction = async function (req, res) {
    const {userToken, serviceName, reactionName, parameters} = req.body;

    let currentUser;
    let currentService;
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        currentUser = user;
    });
    await Service.findOne({name: serviceName, owner: currentUser._id}).then(function (service) {
        if (service === null)
            return res.status(400).send("service not found");
        currentService = service;
    }).catch();

    let reactionModel;
    let serviceModel;
    await Service.findOne({name: serviceName, owner: "area"}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(400).send("service not found");
        serviceModel = service;
    });
    await Reaction.find({owner: serviceModel._id}).then(function (reactions) {
        for (let i = 0; i < reactions.length; i++) {
            if (reactions[i].name === reactionName) {
                reactionModel = reactions[i];

            }
        }
    });

    if (reactionModel === undefined)
        return res.status(400).send("Invalid reaction");

    let newReaction = new Reaction({
        name: reactionName,
        owner: currentService._id,
        description: reactionModel.description,
        route: reactionModel.route,
        parameters: parameters // JSON.stringify(parameters)
    });
    newReaction.save();
    currentService.reactionsid.push(newReaction._id);
    currentService.save();

    res.status(200).send(newReaction._id);

};

/**
 * @route GET /user/action
 * @group action
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} actionId.x-www-form-urlencoded - action name to id
 * @return {object} 200 - action to json
 * @return {object} 200 - all action to json
 * @return {Error} 400 - Error
 */
exports.getAction = async function (req, res) {
    const {userToken, actionId} = req.query;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        if (actionId !== undefined)
            Action.findOne({_id: actionId}).then(function (action) {
                if (action === null)
                    return res.status(400).send("No reaction " + actionId);
                return res.status(200).json(action);
            }).catch();
        else {
            Service.find({owner: user._id}).then(function (services) {
                if (services.length === 0)
                    return res.status(400).send("No service available for user");
                for (let i = 0; i < services.length; i++) {
                    Action.find({owner: services[i]._id}).then(function (actions) {
                        return res.json(actions);
                    });
                }
            });
        }
    });
};

/**
 * @route GET /user/reaction
 * @group reaction
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} reactionId.x-www-form-urlencoded - reaction name to id
 * @returns {object} 200 - reaction to json
 * @returns {object} 200 - all reaction to json
 * @returns {Error} 400 - Error
 */
exports.getReaction = async function (req, res) {
    const {userToken, reactionId} = req.query;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        if (reactionId !== undefined) {
            Reaction.findOne({_id: reactionId}).then(function (reaction) {
                if (reaction === null)
                    return res.status(400).send("No reaction " + reactionId);
                return res.status(200).json(reaction);
            }).catch();
        } else {
            Service.find({owner: user._id}).then(function (services) {
                if (services.length === 0)
                    return res.status(400).send("No service available for user");
                for (let i = 0; i < services.length; i++) {
                    Reaction.find({owner: services[i]._id}).then(function (reactions) {
                        return res.json(reactions);
                    });
                }
            });
        }
    });
};

/**
 * @route DELETE /user/action/delete
 * @group action
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} actionId.x-www-form-urlencoded - action name to id
 * @returns {object} 200 - action deleted
 * @returns {Error} 400 - Error
 */
exports.deleteAction = async function (req, res) {
    const {userToken, actionId} = req.body;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        Action.findOne({_id: actionId}).then(function (action) {
            if (action === undefined || action === null)
                return res.status(400).send("Invalid actionid");
            Service.findOne({_id: action.owner}).then(function (service) {
                if (service === undefined)
                    return res.status(400).send("Error");
                if (service.owner === user._id.toString()) {
                    for (let i = 0; i < service.actionid.length; i++) {
                        if (service.actionid[i] === actionId)
                            service.actionid[i].pop();
                    }
                    service.save();
                    action.delete();
                    return res.status(200).send("action deleted");
                }
                return res.status(401).send("User trying to delete action he does not own");
            })
        })
    })
};

/**
 * @route DELETE /user/reaction/delete
 * @group reaction
 * @param {string} userToken.x-www-form-urlencoded.required - user's authentification token
 * @param {string} reactionId.x-www-form-urlencoded - reaction name to id
 * @returns {object} 200 - reaction deleted
 * @returns {Error} 400 - Error
 */
exports.deleteReaction = async function (req, res) {
    const {userToken, reactionId} = req.body;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        Reaction.findOne({_id: reactionId}).then(function (reaction) {
            if (reaction === undefined || reaction === null)
                return res.status(400).send("Invalid reactionid");
            Service.findOne({_id: reaction.owner}).then(function (service) {
                if (service === undefined)
                    return res.status(400).send("Error");
                if (service.owner === user._id.toString()) {
                    for (let i = 0; i < service.reactionid.length; i++) {
                        if (service.reactionid[i] === reactionId)
                            service.reactionid[i].pop();
                    }
                    reaction.delete();
                    service.save();
                    return res.status(200).send("reaction deleted");
                }
                return res.status(401).send("User trying to delete reaction he does not own");
            })
        })
    })
};

/**
 * @route GET /user/area
 * @group area
 */
exports.getArea = async function (req, res) {

    const {userToken} = req.query;

    console.log("req",req)
    //console.log("req.query",req.query)

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        Area.find({owner: user._id}).catch(err => console.log(err)).then(function (areas) {
            res.status(200).json(areas);
        })
    })
};

/**
 * @route POST /user/area/new
 * @group area
 */
exports.newArea = async function (req, res) {
    const {userToken, actionId, reactionId} = req.body;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send("Invalid token");
        let newArea = new Area({
            owner: user._id,
            actionid: actionId,
            reactionid: reactionId
        });
        newArea.save();
        user.area.push(newArea); //._id);
        user.save();
        return res.status(200).send("AREA Successfully created");
    })
};

/**
 * @route DELETE /user/area/delete
 * @group area
 */
exports.deleteArea = async function (req, res) {
    const {userToken, areaId} = req.query;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(400).send("Invalid token");
        Area.findOne({_id: areaId, owner: user._id}).then(function (area) {
            if (area === undefined || area === null)
                return res.status(400).send("Invalid area");
            Action.find({_id: area.actionid}).then(function (actions) {
                if (action === undefined || action === null)
                    return res.status(400).send("Invalid token");
                Service.findOne({_id: action.owner}).then(function (service) {
                    if (service === undefined)
                        return res.status(400).send("Error");
                    if (service.owner === user._id.toString()) {
                        for (let i = 0; i < service.actionid.length; i++) {
                            if (service.actionid[i] === actions._id)
                                service.actionid[i].pop();
                        }
                        service.save();
                        action.delete();
                    }
                })
            });
            Reaction.find({_id: area.reactionid}).then(function (reaction) {
                Service.findOne({_id: reaction.owner}).then(function (service) {
                    if (service === undefined)
                        return res.status(400).send("Error");
                    if (service.owner === user._id.toString()) {
                        for (let i = 0; i < service.reactionid.length; i++) {
                            if (service.reactionid[i] === reaction._id)
                                service.reactionid[i].pop();
                        }
                        reaction.delete();
                        service.save();
                    }
                })
            });
            area.delete();
        });
        for (let i = 0; i < user.area.length; i++) {
            if (user.area[i].id === areaId)
                user.area[i].pop();
        }
        user.save();
        return res.status(200).send("area deleted");
    })
};
