const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');
const {handleTriggeredArea} = require("../controllers/AreaController");

exports.createAction = async function (req, res) {
    return res.status(200).send();
};

exports.handleAction = async function (req, res) {
    var currentDate = new Date();
    var servHour = currentDate.getHours();
    var servMin = currentDate.getMinutes();
    let services;
    await Service.find({name: "timer"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        services = serv
    }).catch();
    if (services !== undefined) {
        for (let i = 0; i < services.length; i++) {
            if (services[i] !== undefined && services[i].actionsid !== undefined) {
                for (let j = 0; j < services[i].actionsid.length; j++) {
                    Action.findOne({_id: services[i].actionsid[j]._id})
                        .then(function (action) {
                            let params = JSON.parse(action.parameters);
                            if (params.hour == servHour && params.minute == servMin) {
                                handleTriggeredArea(action._id, function (res) {
                                });
                            }
                        },)
                }
            }
        }
    }
};