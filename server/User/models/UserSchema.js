const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
    email: String,
    name: String,
    surname: String,
    password: String,
    authToken: String,
    permission: {},
    userServices: [{id: String}],
    area: [{id: String}]
});


UserSchema.methods.generateAuthToken = function generateAuthToken() {
    return (jwt.sign({_id: this._id}, "AREA"));
};

UserSchema.methods.verifyAuthToken = function verifyAuthToken(token) {
    return (jwt.verify(token, "AREA"));
};

const User = mongoose.model('User', UserSchema);
exports.User = User;
