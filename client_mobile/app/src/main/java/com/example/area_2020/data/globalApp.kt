package com.example.area_2020.data

import android.app.Application

class globalApp : Application() {
    companion object {
        var subdomain = "https://area51.ngrok.io"
        var userToken = ""
        var isConnected = false
        var status = ""
    }

    override fun onCreate() {
        super.onCreate()
    }
}