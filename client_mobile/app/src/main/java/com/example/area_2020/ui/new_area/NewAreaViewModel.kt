package com.example.area_2020.ui.new_area

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NewAreaViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is new area Fragment"
    }
    val text: LiveData<String> = _text
}