'use strict';

const express = require('express'),
    router = express.Router();

const user = require('../controllers/UserController');

router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now(), req.header, req.body);
    next();
});


router.get('/', user.getUser);

router.post('/update', user.updateUserInfo);
router.post('/changePassword', user.changePassword);

router.get('/service', user.service);
router.get('/service/:serviceid', user.serviceByName);
router.post('/service/new', user.newService);
router.delete('/service/delete', user.deleteService);

router.get('/action', user.getAction);
router.get('/reaction', user.getReaction);
router.post('/action/new', user.newAction);
router.post('/reaction/new', user.newReaction);
router.delete('/action/delete', user.deleteAction);
router.delete('/reaction/delete', user.deleteReaction);

router.get('/area', user.getArea);
router.post('/area/new', user.newArea);
router.delete('/area/delete', user.deleteArea);




//TODO
// setup area
// get area all
// get area unique
// delete area
module.exports = router;
