package com.example.area_2020

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.area_2020.data.DataCache
import com.example.area_2020.data.areacreaData
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.globalApp
import okhttp3.HttpUrl
import org.json.JSONArray
import org.json.JSONObject

class ActionActivity : AppCompatActivity() {

    var data : String = areacreaData.Companion.service_act

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_action)

        val txt_title = findViewById<TextView>(R.id.action_title)

        txt_title.setText("Action for " + data)

        var listAction = getListAction(data)

        aff(listAction)
    }

    fun aff(listAction : ArrayList<ModelAction>) {
        val listview = findViewById<ListView>(R.id.listview_action)
        val txt_no_service = findViewById<TextView>(R.id.no_service_set_text)
        val adapter = MyServiceAdapter(this, listAction)
        val onclick = MyServiceClickListener(this, listAction)

        if (listAction.size == 0) {
            txt_no_service.visibility = View.VISIBLE
            listview.visibility = View.INVISIBLE
        } else {
            listview.visibility = View.VISIBLE
            txt_no_service.visibility = View.INVISIBLE
            listview.adapter = adapter
            listview.onItemClickListener = onclick
        }
    }

    inner class MyServiceClickListener(context: Context, listAction: ArrayList<ModelAction>) : AdapterView.OnItemClickListener {
        private val data = listAction

        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            areacreaData.Companion.parameters_act = data[position].parameters
            areacreaData.Companion.name_act = data[position].name
            val intent = Intent(this@ActionActivity,ActionParametersActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    inner class MyServiceAdapter(context : Context, listAction: ArrayList<ModelAction>) : BaseAdapter() {
        private val data = listAction
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val actionItem = layoutInflater.inflate(R.layout.item_actionreaction, viewGroup, false)

            val txt_desc = actionItem.findViewById<TextView>(R.id.action_desc)

            txt_desc.setText(data[position].desc)

            return actionItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }
    }

    fun getListAction(serviceName : String) : ArrayList<ModelAction> {
        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("services").addPathSegment(data).addPathSegment("action").addQueryParameter("userToken", globalApp.Companion.userToken).build()
        println("URL = " +url)
        var response = extern_get(url)
        println("ACTION RESPONSE : " +response)
        if (response.code == 200) {
            var jsonList = ArrayList<ModelAction>()
            var jsonAct = JSONArray(response.mess)
            for (i in 0..(jsonAct.length()-1)) {
                var action = ModelAction("", "", "", "", JSONObject())
                action.id = jsonAct.getJSONObject(i).getString("_id")
                action.owner = jsonAct.getJSONObject(i).getString("owner")
                action.name = jsonAct.getJSONObject(i).getString("name")
                action.desc = jsonAct.getJSONObject(i).getString("description")
                action.parameters = JSONObject(jsonAct.getJSONObject(i).getString("parameters"))

                jsonList.add(action)
            }
            return jsonList
        } else {
            Toast.makeText(this, "Can't access to the server", Toast.LENGTH_SHORT).show()
        }
        return ArrayList<ModelAction>()
    }
}

data class ModelAction(
    var id : String,
    var owner : String,
    var name : String,
    var desc : String,
    var parameters : JSONObject

)
