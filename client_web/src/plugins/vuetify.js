import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdiSvg',
    },
    theme: {
        themes: {
            light: {
                primary: '#3F51B5',
                secondary: '#FFD54F'
            },
            dark: {
                primary: '#FFD54F',
                secondary: '#3F51B5',
                success: '#69F0AE'
            },
        },
    }
});
