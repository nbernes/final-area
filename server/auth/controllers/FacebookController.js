const FacebookStrategy = require('passport-facebook').Strategy
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");

var userToken;

passport.use(new FacebookStrategy({
    clientID: '764486750713877',
    clientSecret: '130a38c1fd423c2a90a46622ab43167e',
    callbackURL: "https://localhost:8080/auth/facebook/callback"
    }, function (accessToken, refreshToken, profile, done) {
        newService(userToken, "facebook", accessToken, refreshToken).then(res => console.log(res)).catch();
        return done(null, true);
    }));

exports.checkAuthentication = async function (req, res) {
    return res.redirect('http://localhost:8081/home');
};

exports.callOAuth = function(req, res) {
    userToken = req.query.userToken;
    passport.authenticate('facebook')(req, res);
};
