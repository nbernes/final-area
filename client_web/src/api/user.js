import axios from 'axios'

const reqHeader = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Access-Control-Allow-Origin": process.env.VUE_APP_PUBLIC_SERVER_URL
};

export default axios.create({
    baseURL: process.env.VUE_APP_LOCAL_SERVER_URL + 'user/',
    headers: reqHeader,
    redirect: 'follow',
    crossDomain: true,
});