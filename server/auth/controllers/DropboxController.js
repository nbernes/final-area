const DropboxOAuth2Strategy = require('passport-dropbox-oauth2').Strategy;
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");
const mongoose = require('mongoose');
const Service = mongoose.model('Service');

var userToken;
var toRedirect;

passport.use(new DropboxOAuth2Strategy({
        apiVersion: '2',
        clientID: 'nitguduh1zzcv8o',
        clientSecret: 'y8ye0mwya4vesa3',
        callbackURL: "http://localhost:8080/auth/dropbox/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        newService(userToken, "dropbox", accessToken, refreshToken, JSON.parse(profile._raw).account_id).then(res => console.log(res)).catch();
        return done(null, true);
    }));

exports.checkAuthentication = async function (req, res) {
    return res.redirect('http://localhost:8081' + toRedirect);
};

exports.callOAuth = function(req, res) {
    userToken = req.query.userToken;
    toRedirect = req.query.toRedirect;
    passport.authenticate('dropbox-oauth2')(req, res);
};
