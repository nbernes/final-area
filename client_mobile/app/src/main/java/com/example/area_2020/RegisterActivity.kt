package com.example.area_2020

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.example.area_2020.communication.extern_post
import com.example.area_2020.data.globalApp
import okhttp3.FormBody

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val link_sign_in = findViewById<TextView>(R.id.go_sign_in)
        val btnregister = findViewById<CardView>(R.id.card_register)
        val et_name = findViewById<EditText>(R.id.re_name)
        val et_mail = findViewById<EditText>(R.id.re_mail)
        val et_pass = findViewById<EditText>(R.id.re_pass)
        val btnback = findViewById<ImageView>(R.id.go_to_param)

        link_sign_in!!.setOnClickListener {
            openSignIn()
        }

        btnback!!.setOnClickListener {
            val intent = Intent(this, BasicParameterActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        btnregister!!.setOnClickListener {
            val mess_name = et_name.text.toString()
            val mess_mail = et_mail.text.toString()
            val mess_pass = et_pass.text.toString()

            if (mess_name.trim().length <= 0) {
                Toast.makeText(this, "Please give a name", Toast.LENGTH_SHORT).show()
            } else if (mess_pass.trim().length <= 7) {
                Toast.makeText(this, "Please give a password (min 8 characters)", Toast.LENGTH_SHORT).show()
            } else if (!mess_mail.isValidEmail()) {
                Toast.makeText(this, "Please give a valid mail", Toast.LENGTH_SHORT).show()
            } else {
                val requestBody = FormBody.Builder().add("email", mess_mail).add("password", mess_pass).add("name", mess_name).build()
                val data = extern_post(globalApp.Companion.subdomain + "/auth/register", requestBody)
                if (data.code == 200) {
                    Toast.makeText(this, "Succesful Registration", Toast.LENGTH_SHORT).show()
                    openSignIn()
                } else if (data.code == 404) {
                    Toast.makeText(this, "Can't find the server", Toast.LENGTH_SHORT).show()
                } else if (data.code == -1){
                    Toast.makeText(this, "Thread Error", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Name or Username Already Take", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun String.isValidEmail() =
        this.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun openSignIn() {
        val intent = Intent(this, SignInActivity::class.java)
        startActivity(intent)
        this.finish()
    }
}