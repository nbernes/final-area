const mongoose = require('mongoose');

const ServiceSchema = new mongoose.Schema({
    owner: String,
    userid: String,
    name: String,
    description: String,
    connectionRequired: Boolean,
    logo: String,
    token: String,
    accessToken: String,
    refreshToken: String,
    actionsid: [ {id : String } ],
    reactionsid: [ {id : String } ],

});

const Service = mongoose.model('Service', ServiceSchema);
exports.Service = Service;