package com.example.area_2020.ui.home

import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.area_2020.MainActivity
import com.example.area_2020.R
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.globalApp
import com.squareup.picasso.Picasso
import okhttp3.HttpUrl
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text
import java.net.URL

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        // REFRESH BUTTON

        val refreshbtn = root.findViewById<ImageView>(R.id.refresh)

        refreshbtn!!.setOnClickListener {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow()
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow()
        }

        // BUILD LIST OF AREA

        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("area").addQueryParameter("userToken", globalApp.Companion.userToken).build()
        var response = extern_get(url)
        if (response.code == 200) {
            var jsonArr = JSONArray(response.mess)

            url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("services")
                .addPathSegment("list")
                .addQueryParameter("userToken", globalApp.Companion.userToken).build()
            response = extern_get(url)
            var list = JSONArray(response.mess)
            var listArea: ArrayList<ModelArea> = ArrayList<ModelArea>()

            for (jsonIndex in 0..(jsonArr.length() - 1)) {

                val action =
                    get_action(jsonArr.getJSONObject(jsonIndex).getString("actionid"), scheme, host)
                val reaction = get_reaction(
                    jsonArr.getJSONObject(jsonIndex).getString("reactionid"), scheme, host
                )

                if (action != null && reaction != null) {
                    var nameAct: String = ""
                    var descAct: String = ""
                    var logoAct: String = ""
                    var nameReact: String = ""
                    var descReact: String = ""
                    var logoReact: String = ""
                    var idArea = jsonArr.getJSONObject(jsonIndex).getString("_id")

                    url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user")
                        .addPathSegment("service")
                        .addQueryParameter("userToken", globalApp.Companion.userToken).build()
                    response = extern_get(url)
                    var jsonServ = JSONArray(response.mess)

                    var area: ModelArea

                    for (i in 0..(jsonServ.length() - 1)) {
                        if (jsonServ.getJSONObject(i).getString("_id") == action.getString("owner")
                        ) {
                            nameAct = action.getString("name")
                            try {
                                descAct = action.getString("description")
                            } catch (e: JSONException) {
                                descAct = ""
                            }

                            for (j in 0..(list.length() - 1)) {
                                if (jsonServ.getJSONObject(i).getString("name") == list.getJSONObject(
                                        j
                                    ).getString(
                                        "name"
                                    )
                                ) {
                                    logoAct = list.getJSONObject(j).getString("logo")
                                }
                            }
                        }
                        if (jsonServ.getJSONObject(i).getString("_id") == reaction.getString(
                                "owner"
                            )
                        ) {
                            nameReact = reaction.getString("name")
                            try {
                                descReact = reaction.getString("description")
                            } catch (e: JSONException) {
                                descReact = ""
                            }
                            for (k in 0..(list.length() - 1)) {
                                if (jsonServ.getJSONObject(i).getString("name") == list.getJSONObject(
                                        k
                                    ).getString(
                                        "name"
                                    )
                                ) {
                                    logoReact = list.getJSONObject(k).getString("logo")
                                }
                            }
                        }
                    }
                    println("NAME ACTION : " + nameAct)
                    println("NAME REACTION : " + nameReact)
                    println("DESC ACTION : " + descAct)
                    println("DESC REACTION : " + descReact)
                    println("LOGO ACTION : " + logoAct)
                    println("LOGO REACTION : " + logoReact)
                    println("ID AREA : " + idArea)
                    area = ModelArea(
                        nameAct,
                        descAct,
                        logoAct,
                        nameReact,
                        descReact,
                        logoReact,
                        idArea
                    )
                    listArea.add(area)
                }
            }
            aff(listArea, root)
        } else {
            Toast.makeText(activity, "Can't access to the server", Toast.LENGTH_SHORT).show()
        }
        return root
    }

    fun get_action(actionid : String, scheme : String, host : String) : JSONObject? {
        val actionidc = actionid.replace("\"", "")

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("action").addQueryParameter("userToken",  globalApp.Companion.userToken).addQueryParameter("actionId", actionidc) .build()
        var response = extern_get(url)
        if (response.code == 200) {
            val action = JSONObject(response.mess)
            return action
        }
        return null
    }

    fun get_reaction(reactionid:String, scheme: String, host: String) : JSONObject? {
        val reactionidc = reactionid.replace("\"", "")
        val url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("reaction").addQueryParameter("userToken",  globalApp.Companion.userToken).addQueryParameter("reactionId", reactionidc) .build()
        val response = extern_get(url)

        if (response.code == 200) {
            val reaction = JSONObject(response.mess)
            return reaction
        }
        return null
    }

    fun aff(listArea: ArrayList<ModelArea>, root: View) {

        val txt = root.findViewById<TextView>(R.id.no_area_text)
        val listview = root.findViewById<ListView>(R.id.listview_existing_area)
        val adapter = MyOwnAdapter(context as MainActivity, listArea)

        if (listArea.size == 0) {
            txt.visibility = View.VISIBLE
            listview.visibility = View.INVISIBLE
        } else {
            txt.visibility = View.INVISIBLE
            listview.visibility = View.VISIBLE
            listview.adapter = adapter
        }
    }

    inner class MyOwnAdapter(context : Context, listArea: ArrayList<ModelArea>) : BaseAdapter() {

        private val data = listArea
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val existingAreaItem = layoutInflater.inflate(R.layout.item_existing_area, viewGroup, false)

            val txt_nameact = existingAreaItem.findViewById<TextView>(R.id.nameact)
            val txt_descact = existingAreaItem.findViewById<TextView>(R.id.descact)
            val txt_namereact = existingAreaItem.findViewById<TextView>(R.id.namereact)
            val txt_descreact = existingAreaItem.findViewById<TextView>(R.id.descreact)
            val img_act = existingAreaItem.findViewById<ImageView>(R.id.logoact)
            val img_react = existingAreaItem.findViewById<ImageView>(R.id.logoreact)
            val cancel_button = existingAreaItem.findViewById<CardView>(R.id.card_cancel)

            Picasso.get().load(data[position].logoAct).into(img_act)
            Picasso.get().load(data[position].logoReact).into(img_react)
            txt_nameact.setText(data[position].nameAct)
            txt_descact.setText(data[position].descAct)
            txt_namereact.setText(data[position].nameReact)
            txt_descreact.setText(data[position].descReact)

            cancel_button!!.setOnClickListener {
                cancel_action(data[position].id)
            }
            return existingAreaItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

    }

    fun cancel_action(id:String) {
        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]
        println(scheme + host)
        val url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("area").addPathSegment("delete") .addQueryParameter("userToken",  globalApp.Companion.userToken).addQueryParameter("areaId", id).build()

        val response = extern_get(url)
        println("CANCEL RESPONSE = " + response)
        if (response.code == 200) {
            fragmentManager?.beginTransaction()?.detach(this)?.commitNow()
            fragmentManager?.beginTransaction()?.attach(this)?.commitNow()
        } else {
            Toast.makeText(activity, "Can't cancel this action", Toast.LENGTH_SHORT).show()
        }
    }
}

data class ModelArea(
    var nameAct : String,
    var descAct : String,
    var logoAct : String,
    var nameReact : String,
    var descReact : String,
    var logoReact : String,
    var id : String
)
