package com.example.area_2020.ui.new_area

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.area_2020.ActionActivity
import com.example.area_2020.LoginActivity
import com.example.area_2020.MainActivity
import com.example.area_2020.R
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.areacreaData
import com.example.area_2020.data.globalApp
import com.example.area_2020.ui.home.HomeFragment
import com.google.gson.JsonObject
import com.squareup.picasso.Picasso
import okhttp3.HttpUrl
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text

class NewAreaFragment : Fragment() {

    private lateinit var new_areaViewModel : NewAreaViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        new_areaViewModel =
            ViewModelProviders.of(this).get(NewAreaViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_new_area, container, false)

        val serviceTab = getServiceTab()
        aff(serviceTab, root)

        return root
    }

    fun aff(serviceTab : ArrayList<ModelService>, root : View) {
        val txt = root.findViewById<TextView>(R.id.no_service_text)
        val listview = root.findViewById<ListView>(R.id.listview_service)
        val adapter = MyServiceAdapter(context as MainActivity, serviceTab)
        val clicklistener = MyOwnClickListener(context as MainActivity, serviceTab)

        if (serviceTab.size == 0) {
            txt.visibility = View.VISIBLE
            listview.visibility = View.INVISIBLE
        } else {
            txt.visibility = View.INVISIBLE
            listview.visibility = View.VISIBLE
            listview.adapter = adapter
            listview.onItemClickListener = clicklistener
        }
    }

    inner class MyOwnClickListener(context: Context, serviceTab: ArrayList<ModelService>) : AdapterView.OnItemClickListener {
        private val data = serviceTab

        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            if (data[position].isConnected == false) {
                areacreaData.Companion.title_connect = data[position].name
                areacreaData.Companion.creafrom = 1
                val intent = Intent(activity, LoginActivity::class.java)
                startActivity(intent)
                activity?.finish()
            } else {
                areacreaData.Companion.service_act = data[position].name
                val intent = Intent(activity,ActionActivity::class.java)
                startActivity(intent)
            }
        }
    }

    inner class MyServiceAdapter(context : Context, serviceTab: ArrayList<ModelService>) : BaseAdapter() {
        private val data = serviceTab
        private val mContext : Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val serviceItem = layoutInflater.inflate(R.layout.item_service, viewGroup, false)

            val txt_name = serviceItem.findViewById<TextView>(R.id.servicename)
            val txt_status = serviceItem.findViewById<TextView>(R.id.connectstatus)
            val img = serviceItem.findViewById<ImageView>(R.id.servicelogo)

            Picasso.get().load(data[position].image).into(img)
            txt_name.setText(data[position].name)

            if (data[position].isConnected == false) {
                txt_status.setText("Not Connected")
                txt_status.setTextColor(Color.RED)
            } else {
                txt_status.setText("Connected")
                txt_status.setTextColor(Color.GREEN)
            }

            return serviceItem
        }

        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return data.size
        }

    }

    fun getServiceTab() : ArrayList<ModelService> {
        val arrayService : ArrayList<ModelService> = ArrayList()
        val tab =  globalApp.Companion.subdomain.split("://").toTypedArray()
        val scheme = tab[0]
        val host = tab[1]

        var url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("services").addPathSegment("list").build()
        var response = extern_get(url)


        if (response.code == 200) {
            println("LIST SERVICE : " + response.mess)
            val jsonservice = JSONArray(response.mess)
            for (i in 0..(jsonservice.length() -1)) {
                var service : ModelService = ModelService("", "", "", false)

                service.name = jsonservice.getJSONObject(i).getString("name")
                service.image = jsonservice.getJSONObject(i).getString("logo")
                service.isConnected = false
                println("this is a service" + service)
                arrayService.add(service)
            }
            url = HttpUrl.Builder().scheme(scheme).host(host).addPathSegment("user").addPathSegment("service").addQueryParameter("userToken", globalApp.Companion.userToken).build()
            response = extern_get(url)
            if (response.code == 200) {
                val jsonuserservice = JSONArray(response.mess)
                for (i in 0..(jsonuserservice.length() -1)) {
                    for (j in 0..(arrayService.size - 1)) {
                        if (jsonuserservice.getJSONObject(i).getString("name") == arrayService[j].name) {
                            arrayService[j].isConnected = true
                        }
                    }
                }
            } else {
                Toast.makeText(activity, "Can't access to the server", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(activity, "Can't access to the server", Toast.LENGTH_SHORT).show()
        }
        return arrayService
    }
}

data class ModelService (
    var name : String,
    var image : String,
    var connectionRequired : String,
    var isConnected : Boolean
)