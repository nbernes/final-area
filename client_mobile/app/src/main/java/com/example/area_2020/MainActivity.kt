package com.example.area_2020

import android.content.Intent
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.area_2020.data.DataCache
import com.example.area_2020.data.globalApp

class MainActivity : AppCompatActivity() {

    private var dataCache : DataCache? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataCache = DataCache(this)

        if (globalApp.Companion.status == "dc") {
            globalApp.status = ""
            dataCache!!.putIsLogin("false")
            dataCache!!.putUserToken("")
        }

        if (dataCache!!.getIsLogin() == "false" && globalApp.Companion.isConnected == false) {
            val intent = Intent(this@MainActivity, SignInActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        if (dataCache!!.getIsLogin() == "true") {
            globalApp.Companion.isConnected = true
            globalApp.Companion.userToken = dataCache!!.getUserToken()!!
        } else {
            println(dataCache!!.getIsLogin())
        }

        println("CACHE DATA TOKEN = " + dataCache!!.getUserToken())
        println("CACHE DATA IS CONNECTED = " + dataCache!!.getIsLogin())
        println("GLOBAL APP IS CONNECTED = " + globalApp.Companion.isConnected)
        println("GLOBAL APP SUBDOMAIN = " + globalApp.Companion.subdomain)
        println("GLOBAL APP TOKEN = " + globalApp.Companion.userToken)

        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_new_area, R.id.navigation_parameters))
        //setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }
}
