const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const about = require('./about.json');
const mongoose = require('mongoose');
const User = require('./User/models/UserSchema');
const reaction = require('./Services/models/ReactionSchema');
const action = require('./Services/models/ActionSchema');
const service = require('./Services/models/ServicesSchema');
const area = require('./Services/models/AreaSchema');
const serviceApi = require('./Services/routes/ServicesRoutes');
const auth = require('./auth/routes/authRoutes');
const bodyParser = require('body-parser');
const serviceInit = require('./Services/controllers/ServiceInfoLoad');
const userApi = require('./User/routes/UserRoutes');
const expressSwagger = require('express-swagger-generator')(app);
const dotenv = require('dotenv');
const weather = require('./Services/Weather/WeatherController');
const timer = require('./Services/Timer/TimerController');

dotenv.config();
let options = {
    swaggerDefinition: {
        info: {
            description: 'AREA Doc',
            title: 'AREA Doc',
            version: '1.0.0',
        },
        host: 'localhost:8080',
        basePath: '/',
        produces: [
            "application/x-www-form-urlencoded",
            "application/json",
            "application/xml"
        ],
        consumes: [
            "application/x-www-form-urlencoded",
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./User/controllers/**/*.js', './Services/controllers/**/*.js', './auth/controllers/**/*.js'] //Path to the API handle folder
};
expressSwagger(options);


const ngrok = require('ngrok');
const ngrokAuthToken = '1XuhByLi4NxYvOIeoYm9SOwJLdq_75AnAtVZ3Y4GCGvqFEqfD';

// mongoose.connect('mongodb://localhost/AREA', {
mongoose.connect('mongodb://db:27018/AREA', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => console.log("Connected to MongoDB..."))
    .catch(err => console.error("Could not connect to MongoDB..."));

const db = mongoose.connection;

db.on("error", () => {
    console.log("> error occurred from the database");
});
db.once("open", () => {
    console.log("> successfully opened the database");
});


app.get('/', function (req, res) {
    res.send('<b>AREA 51</b><br>    NO TRESPASSING');
});

serviceInit();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/about.json', (req, res) => res.send(about));

var logger = require('express-logger');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var inspect = require('util-inspect');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger({ path: "log/express.log"}));
app.use(cookieParser());
app.use(session({ secret: "very secret", resave: false, saveUninitialized: true}));

app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
});

app.use(express.json());

app.use('/services', serviceApi);
app.use('/user', userApi);
app.use('/auth', auth);

app.use(function (req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

setInterval(function() {
    timer.handleAction();
    // weather.handleAction();
}, 30000);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

/* NGROK */
(async function () {
    const url = await ngrok.connect({
        authtoken: ngrokAuthToken,
        addr: process.env.PORT,
        subdomain: process.env.SERVER_DOMAIN
    }).catch(err => console.log(err));
    console.log(url);
})();

ngrok.disconnect(); // stops all
ngrok.kill();
