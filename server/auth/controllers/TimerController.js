const {newService} = require("../../User/controllers/UserController");
exports.addService = async function (req, res) {
    const {userToken, toRedirect} = req.query;

    await newService(userToken, "timer", "", "");
    return res.redirect('http://localhost:8081/' + toRedirect);
};