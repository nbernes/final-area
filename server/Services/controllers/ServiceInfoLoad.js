const serviceInfo = require('../../about.json');
const {Reaction} = require("../models/ReactionSchema");
const Service = require('mongoose').models.Service;
const Action = require('mongoose').models.Action;

function initServicesInfo() {

    console.log("init service info");
    for (let service of serviceInfo.server.services) {

        Service.countDocuments({name: service.name}, function (err, count) {
            if (!err) {
                console.log(count, service.name);
                if (count === 0) {
                    let newService = new Service({
                        owner: "area",
                        name: service.name,
                        description: service.description,
                        connectionRequired: (service.connectionRequired === "true"),
                        logo: service.logo
                    });

                    if (service.actions !== undefined) {
                        for (let i = 0; i < service.actions.length; i++) {
                            let actionService = new Action({
                                owner: newService._id,
                                name: service.actions[i].name,
                                description: service.actions[i].description,
                                parameters: JSON.stringify(service.actions[i].parameters)
                            });
                            newService.actionsid.push(actionService._id);
                            actionService.save();
                        }
                    }
                    if (service.reactions !== undefined) {
                        for (let i = 0; i < service.reactions.length; i++) {
                            let reactionService = new Reaction({
                                owner: newService._id,
                                name: service.reactions[i].name,
                                route: service.reactions[i].route,
                                description: service.reactions[i].description,
                                parameters: JSON.stringify(service.reactions[i].parameters)
                            });
                            newService.reactionsid.push(reactionService._id);
                            reactionService.save();
                        }
                    }
                    newService.save();
                }

            }
        });
    }
}

module.exports = initServicesInfo;



