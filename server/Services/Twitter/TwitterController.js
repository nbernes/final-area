const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction');

var compose = require('request-compose');
compose.Request.oauth = require('request-oauth');
var request = compose.client;
const querystring = require('querystring');
const r = require('request');

let bearerToken;

const crypto = require('crypto');
const OAuth = require('oauth-1.0a');

function hash_function_sha1(base_string, key) {
    return crypto
        .createHmac('sha1', key)
        .update(base_string)
        .digest('base64')
}

var oauth;
var token;

async function deleteWebhook(webhookId) {
    const request_data = {
        url: 'https://api.twitter.com/1.1/account_activity/all/' + process.env.TWITTER_WEBHOOK_ENV + '/webhooks/' + webhookId + '.json',
        method: 'DELETE'
    };
    token = {
        key: process.env.TWITTER_ACCESS_TOKEN,
        secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
    };
    oauth = OAuth({
        consumer: {key: process.env.TWITTER_CONSUMER_KEY, secret: process.env.TWITTER_CONSUMER_SECRET},
        signature_method: 'HMAC-SHA1',
        realm: 'area',
        hash_function: hash_function_sha1,
    });
    let {body} = request({
        method: request_data.method,
        url: request_data.url,
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: token.key,
            token_secret: token.secret,
        }
        //form: oauth.authorize(request_data, token),
    }, function (err, resp, body) {
        console.log(body.body);
    });
    console.log("delete answer = " + body);
}

async function createWebhook() {
    const request_data = {
        method: 'POST',
        url: 'https://api.twitter.com/1.1/account_activity/all/' + process.env.TWITTER_WEBHOOK_ENV + '/webhooks.json?url=' + 'https%3A%2F%2Farea_5.ngrok.io%2Fservices%2Ftwitter%2Fwebhook',
    };

    oauth = OAuth({
        consumer: {key: process.env.TWITTER_CONSUMER_KEY, secret: process.env.TWITTER_CONSUMER_SECRET},
        signature_method: 'HMAC-SHA1',
        realm: 'area',
        hash_function: hash_function_sha1,
    });
    token = {
        key: process.env.TWITTER_ACCESS_TOKEN,
        secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
    };
    console.log(oauth.toHeader(oauth.authorize(request_data, token)));
    let {body} = await request({
        method: request_data.method,
        url: request_data.url,
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: token.key,
            token_secret: token.secret,
        }
        //headers: oauth.toHeader(oauth.authorize(request_data, token))
        /*headers: {
            authorization: {'OAuth realm=' oauth.authorize(request_data, token).oauth_consumer_key,
                oauth_nonce: oauth.authorize(request_data, token).oauth_nonce,
                oauth_signature: oauth.authorize(request_data, token).oauth_signature,
                oauth_signature_method: oauth.authorize(request_data, token).oauth_signature_method,
                oauth_timestamp: oauth.authorize(request_data, token).oauth_timestamp,
                oauth_token: oauth.authorize(request_data, token).oauth_token,
                oauth_version: oauth.authorize(request_data, token).oauth_version}
        }*/

    }, function (err, res, req) {
        if (res.headers === 131)
            createWebhook();
        console.log(res);
    });
}

async function getExistingWebhook(err, res) {
    bearerToken = JSON.parse(res.toString()).access_token;
    console.log("Bearer token " + bearerToken);
    try {
        let {body} = await r({
            method: 'GET',
            url: 'https://api.twitter.com/1.1/account_activity/all/webhooks.json',
            headers: {
                Authorization: 'Bearer ' + bearerToken
            }
        }, function (err, res, req) {

            console.log("Get callback " + res.body);
            if (res.body.webhooks)
                deleteWebhook(res.body.webhooks[0].id);
            createWebhook();
        });

    } catch (e) {
        console.error("get err " + e);
    }
}

exports.initTwitterWebhook = async function () {
    console.log(token);
    console.log(oauth);
    try {
        /*
                          const credentials = `${process.env.TWITTER_CONSUMER_KEY}:${process.env.TWITTER_CONSUMER_SECRET}`;
                            const credentialsBase64Encoded = new Buffer(credentials).toString('base64');

                            await r({
                                url: 'https://api.twitter.com/oauth2/token',
                                method: 'POST',
                                headers: {
                                    'Authorization': `Basic ${credentialsBase64Encoded}`,
                                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                                },
                                body: 'grant_type=client_credentials'
                            }, function (err, resp, body) {
                                getExistingWebhook(err, body);
                            });
        */

        /*try {
    let {body} = await request({
        method: 'POST',
        url: 'https://api.twitter.com/1.1/account_activity/all/' + process.env.TWITTER_WEBHOOK_ENV + '/webhooks.json?' + querystring.stringify(process.env.SERVER_URL + 'services/twitter/webhook'),
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: process.env.TWITTER_ACCESS,
            token_secret: process.env.TWITTER_ACCESS_SECRET
        }
    });
    console.log(body);
} catch (e) {
    console.log(e);
}*/
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
};

function get_challenge_response(crc_token, consumer_secret) {

    let hmac = crypto.createHmac('sha256', consumer_secret).update(crc_token).digest('base64')

    return hmac
};

exports.handleWebhook = async function (req, res) {
    console.log(req);
    var crc_token = request.query.crc_token;
    res.send({
        response_token: 'sha256=' + get_challenge_response(crc_token, process.env.TWITTER_CONSUMER_SECRET)
    });

};

exports.handleReaction = async function (req, res) {
    const reactionId = req.params.reactionid;

    let reaction;
    await Reaction.findOne({_id: reactionId}).then(function (react) {

        if (react === undefined || react === null)
            return res.status(400).send("Reaction not found");
        reaction = react;
    }).catch();
    let service;
    await Service.findOne({_id: reaction.owner}).then(function (serv) {
        if (serv === undefined || serv === null)
            return res.status(400).send("Error service not found");
        service = serv;
    });

    let rand = Math.random().toString(25).substring(3);
    let p = JSON.parse(reaction.parameters);
    p.status += " #" + rand;
    let searchParams = querystring.stringify(p);
    console.log(searchParams);
    const request_data = {
        url: 'https://api.twitter.com/1.1/' + reaction.route + '?' + searchParams,
        method: 'POST'
    };

    token = {
        key: service.accessToken,
        secret: service.refreshToken,
    };
    try {
        await r({
            method: request_data.method,
            url: request_data.url,
            oauth: {
                consumer_key: process.env.TWITTER_CONSUMER_KEY,
                consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
                token: token.key,
                token_secret: token.secret,
            }
        }, function (err, res, req) {
            console.log(req)
        });
    } catch (e) {
        console.log(e);
    }
};

