package com.example.area_2020.ui.parameters

import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.area_2020.MainActivity
import com.example.area_2020.R
import com.example.area_2020.data.DataCache
import com.example.area_2020.data.globalApp

class ParametersFragment : Fragment() {

    private lateinit var parametersViewModel : ParametersViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        parametersViewModel =
            ViewModelProviders.of(this).get(ParametersViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_parameters, container, false)
        val txt = root.findViewById<TextView>(R.id.tv_subdomain)
        val edit = root.findViewById<EditText>(R.id.new_subdomain)
        val btnsub = root.findViewById<CardView>(R.id.card_param)
        val btndc = root.findViewById<CardView>(R.id.card_dc)
        txt.setText(globalApp.Companion.subdomain)

        btnsub!!.setOnClickListener {
            val edit_txt = edit.text.toString()
            if (edit_txt.trim().length <= 0) {
                Toast.makeText(activity, "Please give a new subdomain", Toast.LENGTH_SHORT).show()
            } else {
                globalApp.Companion.subdomain = edit_txt
                fragmentManager?.beginTransaction()?.detach(this)?.commitNow()
                fragmentManager?.beginTransaction()?.attach(this)?.commitNow()
            }
        }

        btndc!!.setOnClickListener {
            globalApp.Companion.userToken = ""
            globalApp.Companion.isConnected = false
            globalApp.Companion.status = "dc"
            val intent = Intent(activity, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        return root
    }
}