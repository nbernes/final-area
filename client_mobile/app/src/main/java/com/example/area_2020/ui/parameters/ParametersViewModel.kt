package com.example.area_2020.ui.parameters

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ParametersViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is parameters Fragment"
    }
    val text: LiveData<String> = _text
}