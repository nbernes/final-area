const state = {
    email: '',
    userToken: '',
    isAuthenticated: false
};

// getters
const getters = {};

// actions
const actions = {
};

// mutations
const mutations = {
    notifyAuthentication(state, res) {
        state.email = res.email;
        state.userToken = res.userToken;
        state.isAuthenticated = true;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}