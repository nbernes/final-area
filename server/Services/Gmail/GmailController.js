const {google} = require('googleapis');
let Check = require('./gmail')
let Mailer = require('./createMail')
const {OAuth2Client} = require('google-auth-library');
let inside = false;
let clock = Date.now();
const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');
const {handleTriggeredArea} = require("../controllers/AreaController");
require('dotenv').config();
const clientID = process.env.GMAIL_ID_CLIENT
const clientSecret = process.env.GMAIL_SECRET_KEY

exports.reaction = async function (req, res) {
    const reactionId = req.params.reactionid;

    let reaction;
    await Reaction.findOne({_id: reactionId}).then(function (react) {
        if (react === undefined || react === null)
            return res.status(400).send("Reaction not found");
        reaction = react;
    });
    let service;
    await Service.findOne({_id: reaction.owner}).then(function (serv) {
            if (serv === undefined || serv === null) {
                console.log("DIDNT FOUND ANY SERVICES ....")
                return res.status(400).send("Error service not found");
            }
            service = serv;
            let param_send_to = JSON.parse(reaction.parameters).send_to;
            let accessToken = service.accessToken;
            let refreshToken = service.refreshToken;

            const oAuth2Client = new OAuth2Client(
                clientID,
                clientSecret,
                'http://localhost:8080/auth/google/callback'
            );
            oAuth2Client.setCredentials({
                access_token: accessToken,
                refresh_token: refreshToken
            });
            var oauth2 = google.oauth2({
                auth: oAuth2Client,
                version: 'v2'
            });
            oauth2.userinfo.get(
                async (err, res) => {
                    if (err) {
                        console.log(err);
                    } else {
                        let mailer = new Mailer(oAuth2Client, res.data.email, param_send_to, 'mail', "AREA51", "Bonjour, \nUne action s'est produite sur l'area51 !")
                        mailer.makeBody();
                        console.log("LE MAIL A ETE ENVOYE")
                    }
                });
            clock = Date.now();
            res.status(200).send();
        });
};

get_last_mail = function (to, accessToken, refreshToken, param_from_front, callback, param) {
    const oAuth2Client = new OAuth2Client(
        clientID,
        clientSecret,
        'http://localhost:8080/auth/google/callback'
    );

    oAuth2Client.setCredentials({
        access_token: accessToken,
        refresh_token: refreshToken
    });
    var oauth2 = google.oauth2({
        auth: oAuth2Client,
        version: 'v2'
    });
    oauth2.userinfo.get(
        async (err, res) => {
            if (err) {
                console.log(err);
            } else {
                let obj = new Check(oAuth2Client, res);
                await obj.checkForMediumMails("from:" + param_from_front, callback, param)
            }
        });
};

exports.action = function (req, res) {
    let currentTime = Date.now();
    if (inside === true  || currentTime - clock < 2000) {
        res.status(200).send();
        return res;
    }
    inside = true;
    const message = Buffer.from(req.body.message.data, 'base64').toString(
        'utf-8'
    );
    let parsed = JSON.parse(message)
    let accessToken;
    let refreshToken;
    let serviceGoogle;

    Service.findOne({token: parsed.emailAddress}).then(function (service) {
        if (service === null) {
            console.log("DIDNT FOUND ANY SERVICES ....");
            return res.status(200).send("No google service");
        }
        console.log("SERVICE : ");
        console.log(service);
        serviceGoogle = service;
        accessToken = service.accessToken;
        refreshToken = service.refreshToken;
        let mailActions;
        Action.find({owner: serviceGoogle._id}).then(function (actions) {
            if (actions.length === 0) {
                console.log("DIDNT FIND ANY ACTIONS ....");
                return res.status(400).send("No google action for " + serviceGoogle.token);
            }
            mailActions = actions;
            console.log("ACTIONS : ");
            console.log(actions);
            for (let i = 0; i < mailActions.length; i++) {
                let mail;
                try {
                    mail = JSON.parse(mailActions[i].parameters);
                } catch {
                    mail = mailActions[i].parameters;
                }
                console.log(mail);
                get_last_mail(parsed.emailAddress, accessToken, refreshToken, mail.expeditor_watched, logMail, mailActions[i]);
            }

            function logMail(body, Action) {
                let mySubString = body.substring(
                    body.lastIndexOf("<") + 1,
                    body.lastIndexOf(">")
                );
                let currentMail = JSON.parse(Action.parameters);
                if (currentMail.expeditor_watched === mySubString) {
                    handleTriggeredArea(Action._id, function (err) {
                        console.error(err)
                    }).then(r => console.log(r));
                    console.log("GOT AN GMAIL REACTION LINKED TO ANOTHER ACTION, ACTION CALLED.")
                }
                inside = false;
                res.status(200).send();
            }
        }).catch();
    }).catch();
    inside = false;
};