package com.example.area_2020

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.example.area_2020.data.globalApp

class BasicParameterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.basic_parameters)

        val txt = findViewById<TextView>(R.id.tv_subdomain)
        val edit = findViewById<EditText>(R.id.new_subdomain)
        val btn = findViewById<CardView>(R.id.card_param)
        val btnback = findViewById<ImageView>(R.id.arrow_back)

        txt.setText(globalApp.Companion.subdomain)

        btnback!!.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        btn!!.setOnClickListener {
            val edit_txt = edit.text.toString()
            if (edit_txt.trim().length <= 0) {
                Toast.makeText(this, "Please give a new subdomain", Toast.LENGTH_SHORT).show()
            } else {
                globalApp.Companion.subdomain = edit_txt
                startActivity(getIntent())
                finish()
            }
        }
    }
}