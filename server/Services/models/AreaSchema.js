const mongoose = require('mongoose');

const AreaSchema = new mongoose.Schema({
    owner: String,
    actionid: String,
    reactionid: String
});

const Area = mongoose.model('Area', AreaSchema);
exports.Area = Area;