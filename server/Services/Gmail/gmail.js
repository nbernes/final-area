const {google} = require('googleapis');

class Check {
    constructor(auth, data) {
        this.me = data.data.email
        this.gmail = google.gmail({version: 'v1', auth});
        this.auth = auth;
        this.body = 'toto';
    }

    async checkForMediumMails(query, callback, param) {
        await this.gmail.users.messages.list({
            userId: this.me,
            q: query
        }, (err, res) => {
            if (!err) {
                var mails = res.data.messages;
                this.getMail(mails[0].id, callback, param);
            } else {
                console.log(err);
            }
        });
        await Promise.resolve(this.body);
    }

    async getMail(msgId, callback, param) {
        await this.gmail.users.messages.get({
            'userId': this.me,
            'id': msgId
        }, (err, res) => {
            if (!err) {
                this.body = res.data.payload.headers[17].value;
                callback(this.body, param);
            }
        });
        return (this.body);
    }
}

module.exports = Check;
