const bcrypt = require("bcryptjs");
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Area = mongoose.model('Area');
const Action = mongoose.model('Action');
const Service = mongoose.model('Service');
const Reaction = mongoose.model('Reaction');

/**
 * This function register a new user
 * @route POST /user/register
 * @group auth
 * @param {string} name.x-www-form-urlencoded - user's name
 * @param {string} email.x-www-form-urlencoded.required - user's email
 * @param {string} password.x-www-form-urlencoded.required - user's password
 * @returns {object} 200 - user information
 * @returns {Error} 400 - user already registered
 */
exports.register = async function (req, res) {
    await User.findOne({email: req.body.email}).then(function (fullfilled) {
        if (fullfilled !== null)
            return res.status(400).send("User already registered.");
    }).catch(function (unfullfilled) {
        console.log("error :" + unfullfilled);
    });

    const date = new Date().toISOString();
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        dateCreated: date,
        dateLastConnected: date,
    });

    const saltRounds = 10;
    let salt = await bcrypt.genSalt(saltRounds).catch(err => console.log(err));
    newUser.password = await bcrypt.hash(req.body.password, salt);

    let token;
    token = newUser.generateAuthToken();
    newUser.authToken = token;
    await newUser.save();
    res.header("x-auth-token", token);
    res.status(200).send({
        _id: newUser._id,
        name: newUser.name,
        email: newUser.email
    });

};

/**
 * This login the user
 * @route POST /user/login
 * @group auth
 * @param {string} email.x-www-form-urlencoded.required - user's email
 * @param {string} password.x-www-form-urlencoded.required - user's password
 * @returns {object} 200 - user authentification token
 * @returns {Error} 400 - Error
 */
exports.login = async function (req, res) {
    const {email, password} = req.body;

    await User.findOne({email: email}).then(function (user) {
        if (user === null)
            return res.status(401).send("No mail registered.");
        bcrypt.compare(password, user.password, function (err, isMatch) {

            if (err) {
                console.log(err);
            } else if (!isMatch) {
                res.status(401).send("Invalid password.");
            } else {
                res.status(200).send({token: user.authToken});
            }
        });

        /*bcrypt.compare(req.password, fullfilled.password).then(function () {
            res.status(200).send({token: fullfilled.authToken});
        }).catch(function () {
            res.status(401).send("Invalid password.");
        });
        */

    }).catch(function (err) {
        console.log("err");
        res.status(401).send("No mail found.");
    });
};

