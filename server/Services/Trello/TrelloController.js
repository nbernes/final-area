const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');

const querystring = require('querystring');
const request = require('request');
const {handleTriggeredArea} = require("../controllers/AreaController");

const key = "5b0f308c98d08d409f09f6397a95903f";

exports.createWebhook = async function (req, res) {
    const {userToken, actionId} = req.body;
    let currentUser;

    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send('Invalid user');
        currentUser = user;
    }).catch();
    let serviceData;
    await Service.findOne({name: "trello", owner: currentUser._id}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(401).send('No service github for user');
        serviceData = service;
    });
    let actionToCreate;
    await Action.findOne({_id: actionId}).then(function (action) {
        if (action === undefined || action === null)
            return res.status(400).send("Error creating action");
        actionToCreate = action;
    });

    let params = {
        idModel: JSON.parse(actionToCreate.parameters).idModel,
        description: actionId,
        callbackURL: process.env.SERVER_URL + 'services/trello/action/' + actionId,
        key: key,
        token: serviceData.accessToken
    };

    let searchParams = querystring.stringify(params);

    var options = {
        'method': 'POST',
        'url': 'https://api.trello.com/1/webhooks/?' + searchParams,
        'headers': {}
    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
};

exports.handleWebhook = async function (req, res) {
    await handleTriggeredArea(req.params.actionid, function (err) {
        console.log(err);
    }, req.body);
    res.status(200).send();
};

exports.handleCreateWebhook = async function (req, res) {
    res.status(200).send();
};

exports.handleReaction = async function (req, res) {
    const reactionId = req.params.reactionid;

    let reaction;
    await Reaction.findOne({_id: reactionId}).then(function (react) {

        if (react === undefined || react === null)
            return res.status(400).send("Reaction not found");
        reaction = react;
    }).catch();
    let service;
    await Service.findOne({_id: reaction.owner}).then(function (serv) {
        if (serv === undefined || serv === null)
            return res.status(400).send("Error service not found");
        service = serv;
    });

    let reg = new RegExp("/:id");
    let match = reg.exec(reaction.route);
    let parameters = JSON.parse(reaction.parameters);
    if (match !== null) {
        reaction.route = reaction.route.replace(":id", parameters.id);
        delete parameters["id"];
    }
    let requestType = parameters.req;
    delete parameters["req"];
    let request = require('request');
    let options = {
        'method': requestType,
        'url': 'https://api.trello.com/1' + reaction.route,
        'headers': {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(parameters)
    };

    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });


    return res.status(200).send();
};

function requestApi(options, callback) {
    request(options, function (error, response) {
        if (error) throw new Error(error);
        callback(error, response.body);
    });
}

async function getAccessToken(userToken, callback) {
    let currentUser;
    await User.findOne({authToken: userToken}).then(function (err, user) {
        if (user === undefined || user === null)
            return callback(err, user);
        currentUser = user;
    }).catch();

    await Service.findOne({name: "trello", owner: currentUser._id}).then(function (err, service) {
        callback(err, service.accessToken);
    });
}

exports.getBoardsUser = async function (req, res) {
    const {userToken} = req.body;

    let currentUser;
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send('Invalid user');
        currentUser = user;
    }).catch();
    let serviceData;
    await Service.findOne({name: "trello", owner: currentUser._id}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(401).send('No service github for user');
        serviceData = service;
    });
    let params = {
        key: key,
        token: serviceData.accessToken
    };
    let searchParams = querystring.stringify(params);

    var options = {
        'method': 'GET',
        'url': 'https://trello.com/1/members/my/boards?' + searchParams,
        'headers': {}
    };

    requestApi(options, function (err, response) {
        let tab = [];
        let parse = JSON.parse(response)
        for (var i = 0; i < parse.length; i++) {

          let board = {
            id: parse[i].id,
            name: parse[i].name
          }
          tab.push(board);
        }
        res.send(tab);
    });

};

exports.getBoardCards = async function (req, res) {
    const {userToken, boardId} = req.body;

    let currentUser;
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send('Invalid user');
        currentUser = user;
    }).catch();
    let serviceData;
    await Service.findOne({name: "trello", owner: currentUser._id}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(401).send('No service github for user');
        serviceData = service;
    });
    let params = {
        key: key,
        token: serviceData.accessToken
    };
    let searchParams = querystring.stringify(params);

    var options = {
        'method': 'GET',
        'url': 'https://trello.com/1/boards/' + boardId + '/cards?' + searchParams,
        'headers': {}
    };

    requestApi(options, function (err, response) {
      let tab = [];
      let parse = JSON.parse(response)
      for (var i = 0; i < parse.length; i++) {

        let cards = {
          id: parse[i].id,
          name: parse[i].name
        }
        tab.push(cards);
      }
      res.send(tab);

    });
};

exports.getBoardLists = async function (req, res) {
    const {userToken, boardId} = req.body;

    let currentUser;
    await User.findOne({authToken: userToken}).then(function (user) {
        if (user === undefined || user === null)
            return res.status(401).send('Invalid user');
        currentUser = user;
    }).catch();
    let serviceData;
    await Service.findOne({name: "trello", owner: currentUser._id}).then(function (service) {
        if (service === undefined || service === null)
            return res.status(401).send('No service github for user');
        serviceData = service;
    });
    let params = {
        key: key,
        token: serviceData.accessToken
    };
    let searchParams = querystring.stringify(params);

    var options = {
        'method': 'GET',
        'url': 'https://trello.com/1/boards/' + boardId + '/lists?' + searchParams,
        'headers': {}
    };

    requestApi(options, function (err, response) {
      let tab = [];
      let parse = JSON.parse(response)
      for (var i = 0; i < parse.length; i++) {

        let list = {
          id: parse[i].id,
          name: parse[i].name
        }
        tab.push(list);
      }
      res.send(tab);
    });
};
