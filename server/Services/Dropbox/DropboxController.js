const r = require('../routes/ServicesRoutes');
const Dropbox = require('dropbox').Dropbox;
const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const mongoose = require('mongoose');
const Service = mongoose.model('Service');
const Reaction = mongoose.model('Reaction');
const User = mongoose.model('User');
const {handleTriggeredArea} = require("../controllers/AreaController");


exports.enableWebHookURI = async function (req, res) {
    const challenge = req.param('challenge');
    res.status(200).send(challenge);
    return res;
};


/* ACTIONS */
function findUserId(users, userId) {
    for (let i = 0; i < users.length; ++i) {
        console.log("user: " + users[i] + " userId" + userId);
        if (users[i] == userId)
            return true;
    }
    return false;
}

exports.handleWebHook = async function (req, res) {
    let services;
    await Service.find({name: "dropbox"}).then(function (servicesFound) {
        if (servicesFound.length === 0)
            return res.status(400).send('No github services');
        services = servicesFound;
    });
    for (let i = 0; i < services.length; i++) {
        if (findUserId(req.body.list_folder.accounts, services[i].userid)) {
            let actions = services[i].actionsid;
            for (let j = 0; j < actions.length; j++) {
                console.log('exec action');
                await handleTriggeredArea(actions[j]._id, function (err) {
                    console.log(err);
                });
            }
            return res.status(200);
        }
    }
    return res.status(200);
};

/* REACTIONS */
/* params: path to append
* */
exports.handleReaction = async function (req, res) {
    const reactionId = req.params.reactionid;

    let reaction;
    await Reaction.findOne({_id: reactionId}).then(function (react) {

        if (react === undefined || react === null)
            return res.status(400).send("Reaction not found");
        reaction = react;
    }).catch(err => console.log(err));
    let service;
    await Service.findOne({_id: reaction.owner}).then(function (serv) {
        if (serv === undefined || serv === null)
            return res.status(400).send("Error service not found");
        service = serv;
    }).catch(err => console.log(err));
    let reg = new RegExp("/:path");
    let match = reg.exec(reaction.route);

    let parameters = JSON.parse(reaction.parameters);

    if (match !== null) {
        reaction.route = reaction.route.replace(":path", parameters.repo);
        delete parameters["path"];
    }
    let request = require('request');
    let options = {
        'method': 'POST',
        'url': 'https://api.dropboxapi.com/2/files' + reaction.route,
        'headers': {
            'Authorization': 'Bearer ' + service.accessToken,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(parameters)
    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log("Hook create");
        console.log(response.body);
    });
    return res.status(200).send();
};