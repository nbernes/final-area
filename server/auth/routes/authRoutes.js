const express = require('express'),
    router = express.Router();

const passport = require("passport");
const auth = require('../controllers/authController');
const dropbox = require("../controllers/DropboxController");
const github = require("../controllers/GithubController");
const facebook = require("../controllers/FacebookController");
const discord = require("../controllers/DiscordController");
const slack = require("../controllers/SlackController");
const trello = require("../controllers/TrelloController");
const google = require("../controllers/GoogleController");
const twitter = require("../controllers/TwitterController");
const timer = require("../controllers/TimerController");
const weather = require("../controllers/WeatherController");

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now(), req.header, req.body);
    next();
});

router.post('/login', auth.login);
router.post('/register', auth.register);


router.get('/timer', timer.addService);
router.get('/weather', weather.addService);

router.use(passport.initialize());

router.use(passport.session());
passport.serializeUser((user, done) => {
    done(null, user);
});
passport.deserializeUser((user, done) => {
    done(null, user);
});

/* DROPBOX */
router.get('/dropbox', dropbox.callOAuth);

router.get('/dropbox/callback',
    passport.authenticate('dropbox-oauth2', {failureRedirect: '/login',
        successRedirect: 'http://localhost:8081/new-area'}));

/* FACEBOOK */
router.get('/facebook', facebook.callOAuth);

router.get('/facebook/callback',
    passport.authenticate('facebook', {failureRedirect: '/login'}),
    facebook.checkAuthentication);

/* DISCORD */
router.get('/discord', discord.callOAuth);

router.get('/discord/callback',
    passport.authenticate('discord', {failureRedirect: '/login'}),
    discord.checkAuthentication);

/* SLACK */
router.get('/slack', slack.callOAuth);

router.get('/slack/callback',
    passport.authenticate('slack', {failureRedirect: '/login'}),
    slack.checkAuthentication);

/* GITHUB */
router.get('/github', github.callOAuth);

router.get('/github/callback',
    passport.authenticate('github', {failureRedirect: '/login'}),
    github.checkAuthentication);

/* GOOGLE */
router.get('/gmail', google.callOAuth);

router.get('/google/callback',
    passport.authenticate('google', {failureRedirect: '/login'}),
    google.checkAuthentication);

/* TRELLO */
router.get('/trello', trello.callOAuth);

router.get('/trello/callback', trello.checkAuthentication);

/* Twitter */
router.get('/twitter', twitter.callOAuth);
router.get('/twitter/callback', twitter.checkAuthentication);
module.exports = router;