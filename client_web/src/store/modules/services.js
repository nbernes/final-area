import servicesAPI from '../../api/services';
import userAPI from '../../api/user';

const state = {
    services: [],
    currentServiceActions: [],
    currentServiceReactions: []
};

// getters
const getters = {
    doesServiceHaveActions: (state) => (serviceTitle) =>
    {
        for (let i = 0; i < state.services.length; ++i) {
            if (state.services[i].title === serviceTitle)
                return state.services[i].actions.length !== 0;
        }
    },
    doesServiceHaveReactions: (state) => (serviceTitle) => {
        for (let i = 0; i < state.services.length; ++i) {
            if (state.services[i].title === serviceTitle)
                return state.services[i].reactions.length !== 0;
        }
    }
};

// mutations
const mutations = {
    clearSessionInfo(state) {
        state.services = [];
        state.currentServiceActions = [];
        state.currentServiceReactions = [];
    }
};

// actions
const actions = {
    getAppServices(context) {
        servicesAPI.get('list')
            .then(res => {
                for (let i = 0; i < res.data.length; ++i) {
                    state.services.push({
                        title: res.data[i].name,
                        image: res.data[i].logo,
                        connectionRequired:res.data[i].connectionRequired,
                        isConnected: false,
                        actions: [],
                        reactions: [],
                    });
                }
                userAPI.get('service',{ params: {userToken: document.cookie }})
                    .then(connectedServices => {
                        for (let i = 0; i < connectedServices.data.length; ++i) {
                            for (let j = 0; j < state.services.length; ++j) {
                                if (connectedServices.data[i].name === state.services[j].title) {
                                    state.services[j].isConnected = true;
                                    break;
                                }
                            }
                        }
                        context.dispatch('getAllServicesActions');
                        context.dispatch('getAllServicesReactions');
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    },
    getAllServicesActions() {
        for (let i = 0; i < state.services.length; ++i) {
            servicesAPI.get(state.services[i].title + '/action', { params: {userToken: document.cookie }})
                .then(res => {
                    for (let j = 0; j < res.data.length; ++j) {
                        servicesAPI.get(state.services[i].title + '/action/' + res.data[j].name, { params: {userToken: document.cookie }})
                            .then(act => {
                                state.services[i].actions.push(act.data[0]);
                                // state.currentServiceActions.push(act.data[0])
                            })
                            .catch(err => console.log(err))
                    }
                })
                .catch(err => console.log(err));
        }
    },
    getAllServicesReactions() {
        for (let i = 0; i < state.services.length; ++i) {
            servicesAPI.get(state.services[i].title + '/reaction', { params: {userToken: document.cookie }})
                .then(res => {
                    for (let j = 0; j < res.data.length; ++j) {
                        servicesAPI.get(state.services[i].title + '/reaction/' + res.data[j].name, { params: {userToken: document.cookie }})
                            .then(react => {
                                state.services[i].reactions.push(react.data[0]);
                                // state.currentServiceActions.push(act.data[0])
                            })
                            .catch(err => console.log(err))
                    }
                })
                .catch(err => console.log(err));
        }
    },
};


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}