const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action');

exports.list = async function (req, res) {
    Service.find({owner: "area"}, function (err, service) {
        if (err)
            res.send(err);
        res.json(service);
    });
};

exports.serviceByName = async function (req, res) {

    console.log("Srvice", Service.find({}))
    await Service.findOne({name: req.query.serviceid, owner: "area"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        res.status(200).json(serv);
    }).catch();

};

exports.serviceReaction = async function (req, res) {

    let serviceId;
    await Service.findOne({name: req.params.serviceid, owner: "area"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        serviceId = serv._id;
    }).catch();
    await Reaction.find({owner: serviceId}).then(reactions => (res.status(200).send(reactions))).catch(error => res.status(200).send("No reaction for service " + req.params.serviceid));
};

exports.serviceAction = async function (req, res) {

    let serviceId;
    await Service.findOne({name: req.params.serviceid, owner: "area"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        serviceId = serv._id;
    }).catch();

    await Action.find({owner: serviceId}).then(actions => res.status(200).send(actions)).catch(error => res.status(200).send("No action for service " + req.params.serviceid));

};

exports.serviceReactionByName = async function (req, res) {

    let serviceId;
    await Service.findOne({name: req.params.serviceid, owner: "area"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        serviceId = serv._id;
    }).catch();
    await Reaction.find({
        owner: serviceId,
        name: req.params.reactionid
    }).then(reactions => (res.status(200).send(reactions))).catch(error => res.status(200).send("No reaction for service " + req.params.serviceid));
};

exports.serviceActionByName = async function (req, res) {

    let serviceId;
    await Service.findOne({name: req.params.serviceid, owner: "area"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        serviceId = serv._id;
    }).catch();
    await Action.find({
        owner: serviceId,
        name: req.params.actionid
    }).then(actions => (res.status(200).send(actions))).catch(error => res.status(200).send("No action for service " + req.params.serviceid));
};