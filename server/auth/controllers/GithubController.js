var GithubStrategy = require('passport-github2').Strategy;
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");

var userToken;
var toRedirect;
passport.use(new GithubStrategy({
    clientID: '9ce01cf298155f4e2aae',
    clientSecret: 'd526ce0e14fd453a2e02e3a86321eabaaddcb278',
    callbackURL: 'http://localhost:8080/auth/github/callback',
}, function (accessToken, refreshToken, profile, done) {
    console.log(profile);
    newService(userToken, "github", accessToken, refreshToken).then(res => console.log(res)).catch();
    return done(null, true);
}));

exports.checkAuthentication = async function (req, res) {
    res.redirect("http://localhost:8081/" + toRedirect);
};

exports.callOAuth = function (req, res) {
    userToken = req.query.userToken;
    toRedirect = req.query.toRedirect;
    passport.authenticate('github',{ scope: [ 'admin:repo_hook', 'repo' ] })(req, res);
};

