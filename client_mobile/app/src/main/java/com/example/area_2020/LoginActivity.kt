package com.example.area_2020

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import com.example.area_2020.data.DataCache
import com.example.area_2020.data.areacreaData
import com.example.area_2020.communication.extern_get
import com.example.area_2020.data.globalApp
import com.example.area_2020.ui.new_area.NewAreaFragment
import okhttp3.HttpUrl
import org.json.JSONArray
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        println("inside")
        val webview = findViewById<WebView>(R.id.webview_login)

        webview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 9; AOSP on IA Emulator Build/PSR1.180720.117) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36")
        webview.getSettings().setJavaScriptEnabled(true)

        val webclt = WebViewClt(this@LoginActivity)
        webview.loadUrl(globalApp.Companion.subdomain + "/auth/" + areacreaData.Companion.title_connect + "?userToken=" + globalApp.Companion.userToken + "&toRedirect=ok")

        webview.webViewClient = webclt
    }
}

class WebViewClt internal constructor(act: Context) : WebViewClient() {
    val mact : Context

    init {
        mact = act
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url);

        println("URL = " + url)
        if (url!!.startsWith("http://127.0.0.1:8080")) {
            val new_url = url.replace("http://127.0.0.1:8080", globalApp.Companion.subdomain)
            view?.loadUrl(new_url)
        }
        if (url!!.startsWith("http://localhost:8080")) {
            val new_url = url.replace("http://localhost:8080", globalApp.Companion.subdomain)
            view?.loadUrl(new_url)
        }
        if (url == ("http://localhost:8081/ok")) {
            if (areacreaData.Companion.creafrom == 1) {
                val intent = Intent(view?.context, MainActivity::class.java)
                view?.context?.startActivity(intent)
            } else if (areacreaData.Companion.creafrom == 2) {
                val intent = Intent(view?.context, ReactionServiceActivity::class.java)
                view?.context?.startActivity(intent)
            }
        }
    }
}