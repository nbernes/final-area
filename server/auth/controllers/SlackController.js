const SlackStrategy = require('passport-slack').Strategy;
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");

var userToken;

passport.use(new SlackStrategy({
    clientID: '910576496916.912893255783',
    clientSecret: '358f231362372f71e1248f6f73a3b438'
}, function (accessToken, refreshToken, profile, done) {
        newService(userToken, "slack", accessToken, refreshToken).then(res => console.log(res)).catch();
        return done(null, true);
    }));

exports.checkAuthentication = async function (req, res) {
    return res.redirect('http://localhost:8081/home');
};

exports.callOAuth = function(req, res) {
    userToken = req.query.userToken;
    passport.authenticate('slack')(req, res);
};
