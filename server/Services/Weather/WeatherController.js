let request = require('request');

const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');
const {handleTriggeredArea} = require("../controllers/AreaController");

exports.createAction = async function (req, res) {
    return res.status(200).send();
};

exports.handleAction = async function (req, res) {
    let services;
    await Service.find({name: "weather"}).then(function (serv) {
        if (serv === null)
            return res.status(400).send("service not found");
        services = serv
    }).catch();
    if (services !== undefined && services !== null) {
        for (let i = 0; i < services.length; i++) {
            if (services[i] !== undefined && services[i] !== null
                && services[i].actionsid !== undefined && services[i].actionsid !== null) {
                for (let j = 0; j < services[i].actionsid.length; j++) {
                    Action.findOne({_id: services[i].actionsid[j]._id})
                        .then(function (action) {
                            let params = JSON.parse(action.parameters);
                            let url = `http://api.openweathermap.org/data/2.5/weather?q=${params.city}&appid=${process.env.WEATHER_CONSUMER_KEY}`;
                            request(url, function (err, response, body) {
                                if (err) {
                                } else {
                                    let weather = JSON.parse(body);
                                    let apiRes = Math.ceil(weather.main.temp - 273);
                                    if (params.temp == apiRes.temp)
                                        handleTriggeredArea(action._id, function (res) {
                                        });
                                }
                            });
                        })
                }
            }
        }
    }
};