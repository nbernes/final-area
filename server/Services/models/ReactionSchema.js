const mongoose = require('mongoose');

const ReactionSchema = new mongoose.Schema({
    owner: String,
    name: String,
    callback: String,
    route: String,
    description: String,
    parameters: String

});

const Reaction = mongoose.model('Reaction', ReactionSchema);
exports.Reaction = Reaction;
