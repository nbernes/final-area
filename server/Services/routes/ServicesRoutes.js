const express = require('express'),
    router = express.Router();
const services = require('../controllers/ServicesController');

/* gmail */
const bodyParser = require('body-parser');
const gmail_controller = require('../Gmail/GmailController');
const jsonBodyParser = bodyParser.json();

/* dropbox */
const dropbox = require('../Dropbox/DropboxController');
const github = require('../Github/GithubController');

const timer = require('../Timer/TimerController');
const weather = require('../Weather/WeatherController');

const trello = require("../Trello/TrelloController");
const twitter = require("../Twitter/TwitterController");


router.use(function timeLog (req, res, next) {
    next();
});

router.get('/list', services.list);
router.get('/:serviceid', services.serviceByName);
router.get('/:serviceid/action', services.serviceAction);
router.get('/:serviceid/reaction', services.serviceReaction);
router.get('/:serviceid/reaction/:reactionid', services.serviceReactionByName);
router.get('/:serviceid/action/:actionid', services.serviceActionByName);

/* GMAIL */
router.post('/gmail/reaction/:reactionid', gmail_controller.reaction);
router.post('/gmail/action', jsonBodyParser, gmail_controller.action);

router.post('/github/action/new', github.createWebHook);
router.post('/github/action', github.handleWebHook);
router.post('/github/action/:actionid', github.handleWebHook);
router.post('/github/reaction/:reactionid', github.handleReaction);

router.post('/timer/action/new', timer.createAction);
router.get('/timer/action', timer.handleAction);

router.get('/weather/action/new', weather.createAction);
router.get('/weather/action', weather.handleAction);


/* TRELLO */
router.post('/trello/action/new', trello.createWebhook);
router.post('/trello/action/:actionid', trello.handleWebhook);
router.head('/trello/action/:actionid', trello.handleCreateWebhook);
router.post('/trello/reaction/:reactionid', trello.handleReaction);

router.get('/trello/user/boards', trello.getBoardsUser);
router.get('/trello/user/boards/cards', trello.getBoardCards);
router.get('/trello/user/boards/lists', trello.getBoardLists);

/* DROPBOX */
router.get('/dropbox/webhook', dropbox.enableWebHookURI);
router.post('/dropbox/webhook', dropbox.handleWebHook);
router.post('/dropbox/reaction/:reactionid', dropbox.handleReaction);

/*
Twitter
 */
//TODO get webhook create webhook save webhook subscribe user crc token challenge
//

router.post('/twitter/webhook', twitter.handleWebhook);
router.get('/twitter/webhook', twitter.handleWebhook);
router.post('/twitter/reaction/:reactionid', twitter.handleReaction);

module.exports = router;

