const GoogleStrategy = require('passport-google-oauth2').Strategy
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");
const {OAuth2Client} = require('google-auth-library');
const {google} = require('googleapis');
const mongoose = require('mongoose'),
    Service = mongoose.model('Service'),
    Reaction = mongoose.model('Reaction'),
    Action = mongoose.model('Action'),
    User = mongoose.model('User');
require('dotenv').config();
const clientId = process.env.GMAIL_ID_CLIENT;
const clientSecret = process.env.GMAIL_SECRET_KEY;
let userToken;
var toRedirect;

passport.use(new GoogleStrategy({
    clientID: clientId,
    clientSecret: clientSecret,
    userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
    callbackURL: 'http://localhost:8080/auth/google/callback',
    scope: ['openid', 'https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.compose', 'https://www.googleapis.com/auth/gmail.send']
}, function (accessToken, refreshToken, profile, done) {
    newService(userToken, "gmail", accessToken, refreshToken).then(res => console.log(res)).catch();
    const oAuth2Client = new OAuth2Client(
        clientId,
        clientSecret,
        'http://localhost:8080/auth/google/callback'
    );
    oAuth2Client.setCredentials({
        refresh_token: refreshToken,
        access_token: accessToken
    });
    var oauth2 = google.oauth2({
        auth: oAuth2Client,
        version: 'v2'
    });
    oauth2.userinfo.get(
        function (err, res) {
            if (err) {
                console.log(err);
            } else {
                console.log("USERTOKEN " + userToken)
                User.findOne({authToken:userToken}).then(function (user) {
                    console.log(user);
                    if (user === null || user === undefined)
                        return console.log("ERROR user ");

                    Service.findOne({owner:user._id, name:"gmail"}).then(function (service) {
                        if (service === null || service === undefined)
                            return console.log("ERROR SERVICE ");
                        console.log("MAIL : " + res.data.email);
                        service.token = res.data.email;
                        service.save();
                    }).catch();
                    let gmail = google.gmail("v1");
                    gmail.users.watch({
                        access_token: accessToken,
                        userId: res.data.email,
                        //q: 'in:inbox is:unread', API GOOGLE BUGGED SINCE 2015 : https://stackoverflow.com/questions/30591302/gmail-api-watch-not-filtering-by-label
                        resource: {//labelId: 'INBOX',
                            //labelFilterAction: 'include',
                            topicName: process.env.GMAIL_TOPIC_NAME}
                    }, function (err, response) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        console.log(response);
                    });
                })

            }
        });
    return done(null, true);
}));

exports.checkAuthentication = async function (req, res) {
    return res.redirect("http://localhost:8081/" + toRedirect);
};

exports.callOAuth = function (req, res) {
    userToken = req.query.userToken;
    toRedirect = req.query.toRedirect;
    passport.authenticate('google')(req, res);
};

