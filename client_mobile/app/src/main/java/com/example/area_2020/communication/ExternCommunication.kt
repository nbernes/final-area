package com.example.area_2020.communication

import android.os.Handler
import android.widget.Toast
import com.example.area_2020.data.DataCom
import okhttp3.*
import java.io.IOException

fun extern_post(url : String, body : RequestBody) : DataCom {
    val client = OkHttpClient()
    val request = Request.Builder().url(url).post(body).header("Content-Type", "application/x-www-form-urlencoded").build()
    var data : DataCom = DataCom(0, "")

    Thread({
        try {
            val response : Response = client.newCall(request).execute()
            data.code = response.code()
            data.mess = response.body()!!.string()
        } catch (e:IOException) {
            e.printStackTrace()
            data.code = -1
            data.mess = "error"
        }
    }).start()
    while (data.code == 0 || data.mess == "") {
        Handler().postDelayed({},100)
    }
    return (data)
}

fun extern_get(url: HttpUrl) : DataCom {
    val client = OkHttpClient()
    val request = Request.Builder().url(url).header("Content-Type", "application/x-www-form-urlencoded") .get().build()
    var data = DataCom(0, "")

    Thread({
        try {
            val response : Response = client.newCall(request).execute()
            data.mess = response.body()!!.string()
            data.code = response.code()
        } catch (e:IOException) {
            e.printStackTrace()
            data.code = -1
            data.mess= "error"
        }
    }).start()
    while (data.code == 0 || data.mess=="") {
        Handler().postDelayed({},100)
    }
    return (data)
}