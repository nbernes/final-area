import Vue from 'vue';
import VueRouter from 'vue-router';
import NewAreaPage from "../views/NewAreaPage";
import ExistingAreasPage from "../views/ExistingAreasPage";
import ProfilePage from "../views/ProfilePage";
import AboutPage from "../views/AboutPage";
import ConnectionPage from "../views/ConnectionPage";
import APKDownloadPage from "../views/APKDownloadPage";
import store from '../store/index'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'CONNECTION',
        component: ConnectionPage
    },
    {
        path: '/new-area',
        name: 'NEW AREA',
        component: NewAreaPage
    },
    {
        path: '/home',
        name: 'EXISTING AREAS',
        component: ExistingAreasPage
    },
    {
        path: '/profile',
        name: 'PROFILE',
        component: ProfilePage
    },
    {
        path: '/about',
        name: 'ABOUT',
        component: AboutPage
    },
    {
        path: '/apk-download',
        name: 'DOWNLOAD APK',
        component: APKDownloadPage
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'CONNECTION' && !store.state.user.isAuthenticated)
        next({name: 'CONNECTION'});
    next();
});

export default router
