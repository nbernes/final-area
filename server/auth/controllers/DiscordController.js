const DiscordOAuth2Strategy = require( 'passport-discord-oauth2' ).Strategy
const passport = require('passport');
const {newService} = require("../../User/controllers/UserController");

var userToken;

passport.use(new DiscordOAuth2Strategy({
    clientID: '666320514330918962',
    clientSecret: 'Ei044x_OofO8R7nI7RN2FLs7MEiY2BPn',
    callbackURL: 'http://localhost:8080/auth/discord/callback',
    scope: ['identify', 'email', /* 'connections', (it is currently broken) */ 'guilds', 'guilds.join']
}, function (accessToken, refreshToken, profile, done) {
        newService(userToken, "discord", accessToken, refreshToken).then(res => console.log(res)).catch();
        return done(null, true);
    }));

exports.checkAuthentication = async function (req, res) {
    return res.redirect('http://localhost:8081/home');
};

exports.callOAuth = function(req, res) {
    userToken = req.query.userToken;
    passport.authenticate('discord')(req, res);
};
