package com.example.area_2020

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.example.area_2020.communication.extern_post
import com.example.area_2020.data.DataCache
import com.example.area_2020.data.globalApp
import com.google.gson.Gson
import okhttp3.FormBody

class SignInActivity : AppCompatActivity() {

    private var dataCache : DataCache? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataCache = DataCache(this)

        setContentView(R.layout.activity_sign_in)

        val link_register = findViewById<TextView>(R.id.go_register)
        val btnsignin = findViewById<CardView>(R.id.card_sign_in)
        val et_name = findViewById<EditText>(R.id.si_name)
        val et_pass = findViewById<EditText>(R.id.si_pass)
        val coche = findViewById<CheckBox>(R.id.stay_signed)
        val btnback = findViewById<ImageView>(R.id.go_to_param)

        link_register!!.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        btnback!!.setOnClickListener {
            val intent = Intent(this, BasicParameterActivity::class.java)
            startActivity(intent)
            this.finish()
        }

        btnsignin!!.setOnClickListener {
            val mess_name = et_name.text.toString()
            val mess_pass = et_pass.text.toString()

            if (mess_name.trim().length <= 0 || mess_pass.trim().length <= 0) {
                Toast.makeText(this, "Informations are missing", Toast.LENGTH_SHORT).show()
            } else {
                val requestBody = FormBody.Builder().add("email", mess_name).add("password", mess_pass).build()

                val data = extern_post(globalApp.Companion.subdomain + "/auth/login", requestBody)
                println(data)
                if (data.code == 200) {
                    var inData = Gson().fromJson(data.mess, Model::class.java)
                    globalApp.Companion.userToken = inData.token
                    globalApp.Companion.isConnected = true
                    if (coche.isChecked) {
                        dataCache!!.putIsLogin("true")
                        dataCache!!.putUserToken(inData.token)
                    }
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    this.finish()
                } else if (data.code == 404) {
                    Toast.makeText(this, "Can't find the server", Toast.LENGTH_SHORT).show()
                } else if (data.code == -1) {
                    Toast.makeText(this, "Thread Error", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

data class Model (
    val token: String
)