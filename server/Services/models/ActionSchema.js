const mongoose = require('mongoose');

const ActionSchema = new mongoose.Schema({
    owner: String,
    name: String,
    webhook: String,
    description: String,
    parameters: String
});

//TODO save webhook id

const Action = mongoose.model('Action', ActionSchema);
exports.Action = Action;
